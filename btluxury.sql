-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jul 03, 2019 at 06:06 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `btluxury`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `carCategory` enum('Sedan','SUV','Coupe','Limousine') NOT NULL,
  `make` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `year` varchar(4) NOT NULL,
  `VIN` varchar(17) NOT NULL,
  `color` varchar(20) NOT NULL,
  `dailyFee` decimal(8,2) NOT NULL,
  `deposit` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `carCategory`, `make`, `model`, `year`, `VIN`, `color`, `dailyFee`, `deposit`) VALUES
(1, 'Sedan', 'BMW', 'M5', '2019', '1C3BT56G5EC312763', 'dark blue', '450.00', '500.00'),
(2, 'Coupe', 'BMW', 'M4', '2017', 'JYA51K008EA092888', 'golden', '400.00', '500.00'),
(3, 'Sedan', 'BMW', 'Alpina B7', '2016', '5LTPW16556FJ45995', 'white', '450.00', '500.00'),
(4, 'SUV', 'BMW', 'X5', '2018', '1G1P75SZ3E7375651', 'dark blue', '350.00', '500.00'),
(5, 'SUV', 'Mercedes', '4X4 Brabus', '2017', '1N4AL21E88N480522', 'white', '2000.00', '5000.00'),
(6, 'SUV', 'BMW', 'X5', '2016', '1HGCR2F55FA112735', 'white', '350.00', '500.00'),
(7, 'SUV', 'BMW', 'X5M', '2016', '1MELM65L9WK629257', 'grey', '350.00', '500.00');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `carId` int(11) NOT NULL,
  `pathPhoto` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `carId`, `pathPhoto`) VALUES
(1, 1, 'images/portfolio/portfolio-1/1.jpg'),
(2, 1, 'images/portfolio/portfolio-1/2.jpg'),
(3, 1, 'images/portfolio/portfolio-1/3.jpg'),
(4, 1, 'images/portfolio/portfolio-1/4.jpg'),
(5, 2, 'images/portfolio/portfolio-2/1.jpg'),
(6, 2, 'images/portfolio/portfolio-2/2.jpg'),
(7, 2, 'images/portfolio/portfolio-2/3.jpg'),
(8, 2, 'images/portfolio/portfolio-2/4.jpg'),
(9, 2, 'images/portfolio/portfolio-2/5.jpg'),
(10, 2, 'images/portfolio/portfolio-2/6.jpg'),
(11, 3, 'images/portfolio/portfolio-3/1.jpg'),
(12, 3, 'images/portfolio/portfolio-3/2.jpg'),
(13, 3, 'images/portfolio/portfolio-3/3.jpg'),
(14, 3, 'images/portfolio/portfolio-3/4.jpg'),
(15, 3, 'images/portfolio/portfolio-3/5.jpg'),
(16, 3, 'images/portfolio/portfolio-3/6.jpg'),
(17, 4, 'images/portfolio/portfolio-4/1.jpg'),
(18, 4, 'images/portfolio/portfolio-4/2.jpg'),
(19, 4, 'images/portfolio/portfolio-4/3.jpg'),
(20, 4, 'images/portfolio/portfolio-4/4.jpg'),
(21, 5, 'images/portfolio/portfolio-5/2.jpg'),
(22, 5, 'images/portfolio/portfolio-5/3.jpg'),
(23, 5, 'images/portfolio/portfolio-5/4.jpg'),
(24, 5, 'images/portfolio/portfolio-5/5.jpg'),
(25, 6, 'images/portfolio/portfolio-6/1.jpg'),
(26, 7, 'images/portfolio/portfolio-7/1.jpg'),
(27, 7, 'images/portfolio/portfolio-7/2.jpg'),
(28, 7, 'images/portfolio/portfolio-7/3.jpg'),
(29, 7, 'images/portfolio/portfolio-7/4.jpg'),
(30, 1, 'images/portfolio/portfolio-1/mini.jpg'),
(31, 2, 'images/portfolio/portfolio-2/mini.jpg'),
(32, 3, 'images/portfolio/portfolio-3/mini.jpg'),
(33, 4, 'images/portfolio/portfolio-4/mini.jpg'),
(34, 5, 'images/portfolio/portfolio-5/mini.jpg'),
(35, 6, 'images/portfolio/portfolio-6/mini.jpg'),
(36, 7, 'images/portfolio/portfolio-7/mini.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `rentalagreements`
--

CREATE TABLE `rentalagreements` (
  `id` int(11) NOT NULL,
  `carId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `startDate` datetime NOT NULL,
  `dueDate` datetime NOT NULL,
  `returnDate` datetime DEFAULT NULL,
  `milageAtStart` float(10,2) DEFAULT NULL,
  `milageAtEnd` float(10,2) DEFAULT NULL,
  `insuranceType` enum('Basic','Premium','Extra	','Personal Insurance') NOT NULL,
  `postTripCleaning` tinyint(1) NOT NULL DEFAULT '0',
  `prepaidFuel` tinyint(1) NOT NULL DEFAULT '0',
  `AllowedOutSideQC` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rentalagreements`
--

INSERT INTO `rentalagreements` (`id`, `carId`, `userId`, `startDate`, `dueDate`, `returnDate`, `milageAtStart`, `milageAtEnd`, `insuranceType`, `postTripCleaning`, `prepaidFuel`, `AllowedOutSideQC`) VALUES
(1, 2, 2, '2019-07-05 12:00:00', '2019-07-07 12:00:00', NULL, NULL, NULL, 'Basic', 0, 0, 0),
(2, 5, 4, '2019-07-03 18:00:00', '2019-07-08 18:00:00', NULL, NULL, NULL, 'Premium', 1, 0, 1),
(3, 3, 6, '2019-07-07 15:00:00', '2019-07-10 15:00:00', NULL, NULL, NULL, 'Premium', 0, 1, 0),
(4, 7, 7, '2019-07-06 14:00:00', '2019-07-07 14:00:00', NULL, NULL, NULL, 'Basic', 0, 0, 0),
(5, 1, 5, '2019-07-12 19:00:00', '2019-07-14 19:00:00', NULL, NULL, NULL, 'Basic', 0, 1, 0),
(6, 3, 6, '2019-07-12 16:00:00', '2019-07-16 16:00:00', NULL, NULL, NULL, 'Premium', 0, 0, 1),
(7, 2, 5, '2019-07-19 20:00:00', '2019-07-21 20:00:00', NULL, NULL, NULL, 'Premium', 1, 0, 0),
(8, 4, 2, '2019-07-22 10:00:00', '2019-07-27 10:00:00', NULL, NULL, NULL, 'Premium', 0, 0, 0),
(9, 7, 6, '2019-07-26 17:00:00', '2019-07-28 17:00:00', NULL, NULL, NULL, 'Basic', 0, 1, 0),
(10, 6, 7, '2019-07-25 15:00:00', '2019-07-28 15:00:00', NULL, NULL, NULL, 'Premium', 0, 0, 1),
(13, 4, 14, '2019-07-05 14:00:00', '2019-07-06 20:00:00', NULL, NULL, NULL, 'Premium', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userFName` varchar(50) DEFAULT NULL,
  `userLName` varchar(60) DEFAULT NULL,
  `driverLicense` varchar(20) DEFAULT NULL,
  `insurancePolicy` int(12) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT 'QC',
  `postalCode` varchar(8) DEFAULT NULL,
  `country` varchar(50) DEFAULT 'Canada',
  `phoneNumber` varchar(12) DEFAULT NULL,
  `mailerConsent` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `isRegistered` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userFName`, `userLName`, `driverLicense`, `insurancePolicy`, `address`, `city`, `province`, `postalCode`, `country`, `phoneNumber`, `mailerConsent`, `email`, `password`, `isAdmin`, `isRegistered`) VALUES
(1, 'Mike', 'Vazovsky', 'V58962-151173-01', NULL, '123 Rue Street', 'Montreal', 'QC', 'H1B 3M8', 'Canada', '514-555-5555', 1, 'mike@vazovsky.com', 'Mike123', 1, 1),
(2, 'Jeremy', 'Black', 'B5478-030585-00', NULL, '555 Oak Street', 'Montreal', 'QC', 'M6F 8B7', 'Canada', '514-333-3333', 1, 'jeremy@black.com', 'Jeremy123', 0, 1),
(3, '', '', '', NULL, '', '', 'QC', '', 'Canada', '', 1, 'johnwalker@gmail.com', '', 0, 0),
(4, 'Sandra', 'White', 'W5896-270273', NULL, '321 Elm Street', 'Montreal', 'QC', 'K7B 5E3', 'Canada', '514-888-8888', 0, 'sandra@white.com', 'Sandra123', 0, 0),
(5, 'Chris', 'Jackson', 'J7890-250670-03', NULL, '789 Any Street', 'Montreal', 'QC', 'C5F 7D3', 'Canada', '514-222-2222', 1, 'chris@jackson.com', 'Chris123', 0, 1),
(6, 'Tomas', 'Green', 'G7860-210475-02', NULL, '1 Spme Street', 'Montreal', 'QC', 'U7T 7D3', 'Canada', '514-111-1111', 1, 'tomas@green.com', 'Tomas123', 0, 1),
(7, 'Jess', 'Morgen', 'M4850-101069-02', NULL, '348 That Street', 'Montreal', 'QC', 'L8U 5G8', 'Canada', '514-888-4444', 1, 'jess@morgen.com', 'Jess123', 0, 1),
(14, 'Kira', 'Mira', 'DS12345678910111', NULL, '123 That street', 'Montreal', 'QC', 'H8H8H8', 'Канада', '514-555-5555', 0, 'kira@gmail.com', NULL, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `VIN` (`VIN`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carId` (`carId`);

--
-- Indexes for table `rentalagreements`
--
ALTER TABLE `rentalagreements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carId` (`carId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `driverLicense` (`driverLicense`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `rentalagreements`
--
ALTER TABLE `rentalagreements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`carId`) REFERENCES `cars` (`id`);

--
-- Constraints for table `rentalagreements`
--
ALTER TABLE `rentalagreements`
  ADD CONSTRAINT `rentalagreements_ibfk_1` FOREIGN KEY (`carId`) REFERENCES `cars` (`id`),
  ADD CONSTRAINT `rentalagreements_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
