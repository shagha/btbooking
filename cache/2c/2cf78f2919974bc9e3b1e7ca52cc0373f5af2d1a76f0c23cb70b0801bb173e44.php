<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* step4.html.twig */
class __TwigTemplate_30cfa60d898a3bbe745e816db70a6c22144307884962f22ada520eec29f7f928 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "step4.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <form class=\"agreement\" method='post'>
    <h1 class=\"steps\">Car Rental Agreement</h1>
 <p><strong>This Car Rental Agreement is entered into between:</strong><br> 
 BT LUXURY CAR RENTAL INC. (“Owner”) and ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["fName"]) ? $context["fName"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lName"]) ? $context["lName"] : null), "html", null, true);
        echo " (“Renter”)<br>
 (collectively the “Parties”) and outlines the respective rights and obligations of the Parties relating to the rental of a car.
 </p>
 
\t<h3>1. IDENTIFICATION OF THE RENTAL VEHICLE</h3>
<p>Owner hereby agrees to rent to Renter a passenger vehicle identified as follows:<br><br>

Make: ";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["make"]) ? $context["make"] : null), "html", null, true);
        echo "<br>
Model: ";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["model"]) ? $context["model"] : null), "html", null, true);
        echo "<br>
Year: ";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["year"]) ? $context["year"] : null), "html", null, true);
        echo "<br>
VIN: ";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["VIN"]) ? $context["VIN"] : null), "html", null, true);
        echo "<br>
Color: \t";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["color"]) ? $context["color"] : null), "html", null, true);
        echo "<br>
 (Hereinafter referred to as “Rental Vehicle”).<br>
</p>
 
 
 
\t<h3>2. RENTAL TERM</h3>
<p>The term of this Car Rental Agreement runs from the date and hour of vehicle pickup as indicated just above the signature line at the bottom of this agreement until the return of the vehicle to Owner, and completion of all terms of this agreement by both Parties. The rental term is as follows:<br><br>

Start date and time: ";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["pickupDateTime"]) ? $context["pickupDateTime"] : null), "html", null, true);
        echo "<br>
End date and time: ";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["returnDateTime"]) ? $context["returnDateTime"] : null), "html", null, true);
        echo "<br>
<strong>Late Fees, 80 CAD per hour, after the first 15 minutes of an hour the fee is counted as a full hour.</strong><br>
The Parties may shorten or extend the estimate term of rental by mutual consent.<br>
</p>
 
 
 
\t<h3>3. SCOPE OF USE</h3>
<p>Renter will use the Rented Vehicle only for personal or routine business use, and operate the Rented Vehicle only on properly maintained roads and parking lots.  Renter will comply with all applicable laws relating to holding of licensure to operate the vehicle, and pertaining to operation of motor vehicles.  Renter will not sublease the Rental Vehicle or use it as a vehicle for hire. <strong>Renter will not take the vehicle outside Quebec.</strong> IF renter wants to take the vehicle outside of Quebec, they need to advise the owner and receive signed permission.<br>
</p>
 
 
  
\t<h3>4. MILEAGE</h3>
<p>Mileage of the Rental Vehicle is at the time of commencement of this Car Rental Agreement.<br>

<strong>Mileage on the vehicle will be limited as follows: 300 kilometres.</strong><br>
Any mileage on the vehicle in excess of this limitation will be subject to an excess mileage surcharge of 2 CAD per kilometre.
<br>
</p>

\t<h3>5. RENTAL FEES</h3>
<p>Renter will pay to Owner rental fees for use of the Rental Vehicle as follows:<br>

<strong>Base fee:  ";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["dailyFee"]) ? $context["dailyFee"] : null), "html", null, true);
        echo "CAD per 24 hours</strong><br>
<strong>Fuel:</strong> Renter obliges to return the vehicle with the same amount of fuel as the original amount. Renter must use premium grade fuel (minimum 91 Octane).
Excess mileage fees as set forth in Paragraph 4, above.<br>
<strong>Guidelines:</strong> No smoking in the car - Cleaning fees will be applied.<br>
Renter must return the vehicle clean or select the post trip cleaning extra. If the car is excessively dirty, a cleaning fee will be applied.<br>
NO racing, drifting, drag racing, tire kicking of any kind. The renter must pay for any damage to the tires/rims. Including, flats, curb rashes, scratched rims, excessive tire wear, etc. <br>
</p>

\t<h3>6. SECURITY DEPOSIT</h3>
<p>Renter will be required to provide a security deposit to Owner in the amount of <strong> ";
        // line 61
        echo twig_escape_filter($this->env, (isset($context["deposit"]) ? $context["deposit"] : null), "html", null, true);
        echo " CAD </strong> (“Security Deposit”) to be used in the event of loss or damage to the Rental Vehicle during the term of this agreement.  Owner may, in lieu of collection of a security deposit, place a hold on a credit card in the same amount.  In the event of damage to the Rental Vehicle, Owner will apply this Security Deposit to defray the costs of necessary repairs or replacement.  If the cost for repair or replacement of damage to the Rental Vehicle exceeds the amount of the Security Deposit, Renter will be responsible for payment to the Owner of the balance of this cost. <br>
</p>

\t<h3>7. INSURANCE</h3>
<p>Renter may purchase insurance from the company or use their own insurance. Paid insurance consists of:<br>

<strong>Basic – 49.95 CAD </strong> – 500 CAD deductible for damages under 1000 CAD.

<strong>Premium – 249.95 CAD </strong>– 2500 CAD deductible for damages under 5000 CAD.

<strong>Extra – 499.95 CAD </strong>– 5000 CAD deductible for damages under 10000 CAD. 

If Renter decides to use their own insurance, they must provide to Owner with proof of insurance that would cover damage to the Rental Vehicle at the time this Car Rental Agreement is signed, as well as personal injury to the Renter, passengers in the Rented Vehicle, and other persons or property.  If the Rental Vehicle is damaged or destroyed while it is in the possession of Renter, Renter agrees to pay any required insurance deductible and also assign all rights to collect insurance proceeds to Owner.<br>
</p>

\t<h3>8. INDEMNIFICATION</h3>
<p>Renter agrees to indemnify, defend, and hold harmless the Owner for any loss, damage, or legal actions against Owner as a result of Renter’s operation or use of the Rented Vehicle during the term of this Car Rental Agreement.  This includes any attorney fees necessarily incurred for these purposes.  Renter will also pay for any parking tickets, moving violations, or other citations received while in possession of the Rented Vehicle. <br>
</p>

\t<h3>9. REPRESENTATIONS AND WARRANTIES</h3>
<p>Owner represents and warrants that to Owner’s knowledge, the Rental Vehicle is in good condition and is safe for ordinary operation of the vehicle.<br>
Renter represents and warrants that Renter is legally entitled to operate a motor vehicle under the laws of this jurisdiction and will not operate it in violation of any laws, or in any negligent or illegal manner.<br>
Renter has been given an opportunity to examine the Rental Vehicle in advance of taking possession of it, and upon such inspection, is not aware of any damage existing on the vehicle other than that notated by separate Existing Damage document.
<br>
</p>

\t<h3>10. JURISDICTION AND VENUE</h3>
<p>In the event of any dispute over this agreement, this Car Rental Agreement will be interpreted by the laws of the Province of Quebec, and any lawsuit or arbitration must be brought in the Province of Quebec.  If any portion of this agreement is found to be unenforceable by a court of competent jurisdiction, the remainder of the agreement would still have full force and effect.
<br>
</p>

\t<h3>11. ENTIRE AGREEMENT</h3>
<p>This Car Rental Agreement constitutes the entire agreement between the Parties with respect to this rental arrangement.  No modification to this agreement can be made unless in writing signed by both Parties.  Any notice required to be given to the other party will be made to the contact information below.
<br>
</p>

      <h3>12. EXTRAS</h3>
      <p><strong>Post trip cleaning:</strong> Return the car hassle free, without worrying about cleaning up after your trip. Does not cover upholstery cleaning, spills, stains, pet hair, or smoke removal. <strong>65 CAD</strong> per trip.<br></p>
      
      <input type=\"checkbox\" name=\"postTripCleaning\" value=\"1\">I want a post trip cleaning<br><br>
      <p><strong>Prepaid refuel:</strong> Save time and make drop-off a breeze by adding this Extra, which allows you to return my car at any fuel level. Price includes up to a full tank of gas. <strong>130CAD</strong> per trip.</p>
        <input type=\"checkbox\" name=\"prepaidFuel\" value=\"1\">I want a prepaid fuel option<br><br>
        Insurance policy:
                                                            <select name=\"insuranceType\">
                                                                <option value=\"\"></option>
                                                                <option value=\"Basic\" ";
        // line 106
        if (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "insuranceType", []) == "Basic")) {
            echo " selected ";
        }
        echo ">Basic plan (CAD \$49.95)</option>
                                                                <option value=\"Premium\" ";
        // line 107
        if (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "insuranceType", []) == "Premium")) {
            echo " selected ";
        }
        echo ">Premium plan (CAD \$249.95)</option>
                                                                <option value=\"Extra\" ";
        // line 108
        if (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "insuranceType", []) == "Extra")) {
            echo " selected ";
        }
        echo ">Extra plan (CAD \$499.95)</option>
                                                            </select>
                                                <span class=\"erromessage\">
                                                      ";
        // line 111
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 112
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "insuranceType", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 114
        echo "                                                      </span><br><br>


                                                <input type=\"checkbox\" name=\"acceptContract\" value=\"accept\"><strong>I fully understand the content of this rental agreement and accepts all terms and conditions of this rental agreement.</strong>
                                                <span class=\"erromessage\">
                                                      ";
        // line 119
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 120
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "acceptContract", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 122
        echo "                                                      </span><br><br>
    <input type=\"submit\" value=\"Set up rental agreement\" class=\"nextBtn\">
</form>
    
       

";
    }

    public function getTemplateName()
    {
        return "step4.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 122,  215 => 120,  213 => 119,  206 => 114,  200 => 112,  198 => 111,  190 => 108,  184 => 107,  178 => 106,  130 => 61,  118 => 52,  91 => 28,  87 => 27,  75 => 18,  71 => 17,  67 => 16,  63 => 15,  59 => 14,  47 => 7,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    <form class=\"agreement\" method='post'>
    <h1 class=\"steps\">Car Rental Agreement</h1>
 <p><strong>This Car Rental Agreement is entered into between:</strong><br> 
 BT LUXURY CAR RENTAL INC. (“Owner”) and {{fName}} {{lName}} (“Renter”)<br>
 (collectively the “Parties”) and outlines the respective rights and obligations of the Parties relating to the rental of a car.
 </p>
 
\t<h3>1. IDENTIFICATION OF THE RENTAL VEHICLE</h3>
<p>Owner hereby agrees to rent to Renter a passenger vehicle identified as follows:<br><br>

Make: {{make}}<br>
Model: {{model}}<br>
Year: {{year}}<br>
VIN: {{VIN}}<br>
Color: \t{{color}}<br>
 (Hereinafter referred to as “Rental Vehicle”).<br>
</p>
 
 
 
\t<h3>2. RENTAL TERM</h3>
<p>The term of this Car Rental Agreement runs from the date and hour of vehicle pickup as indicated just above the signature line at the bottom of this agreement until the return of the vehicle to Owner, and completion of all terms of this agreement by both Parties. The rental term is as follows:<br><br>

Start date and time: {{pickupDateTime}}<br>
End date and time: {{returnDateTime}}<br>
<strong>Late Fees, 80 CAD per hour, after the first 15 minutes of an hour the fee is counted as a full hour.</strong><br>
The Parties may shorten or extend the estimate term of rental by mutual consent.<br>
</p>
 
 
 
\t<h3>3. SCOPE OF USE</h3>
<p>Renter will use the Rented Vehicle only for personal or routine business use, and operate the Rented Vehicle only on properly maintained roads and parking lots.  Renter will comply with all applicable laws relating to holding of licensure to operate the vehicle, and pertaining to operation of motor vehicles.  Renter will not sublease the Rental Vehicle or use it as a vehicle for hire. <strong>Renter will not take the vehicle outside Quebec.</strong> IF renter wants to take the vehicle outside of Quebec, they need to advise the owner and receive signed permission.<br>
</p>
 
 
  
\t<h3>4. MILEAGE</h3>
<p>Mileage of the Rental Vehicle is at the time of commencement of this Car Rental Agreement.<br>

<strong>Mileage on the vehicle will be limited as follows: 300 kilometres.</strong><br>
Any mileage on the vehicle in excess of this limitation will be subject to an excess mileage surcharge of 2 CAD per kilometre.
<br>
</p>

\t<h3>5. RENTAL FEES</h3>
<p>Renter will pay to Owner rental fees for use of the Rental Vehicle as follows:<br>

<strong>Base fee:  {{dailyFee}}CAD per 24 hours</strong><br>
<strong>Fuel:</strong> Renter obliges to return the vehicle with the same amount of fuel as the original amount. Renter must use premium grade fuel (minimum 91 Octane).
Excess mileage fees as set forth in Paragraph 4, above.<br>
<strong>Guidelines:</strong> No smoking in the car - Cleaning fees will be applied.<br>
Renter must return the vehicle clean or select the post trip cleaning extra. If the car is excessively dirty, a cleaning fee will be applied.<br>
NO racing, drifting, drag racing, tire kicking of any kind. The renter must pay for any damage to the tires/rims. Including, flats, curb rashes, scratched rims, excessive tire wear, etc. <br>
</p>

\t<h3>6. SECURITY DEPOSIT</h3>
<p>Renter will be required to provide a security deposit to Owner in the amount of <strong> {{deposit}} CAD </strong> (“Security Deposit”) to be used in the event of loss or damage to the Rental Vehicle during the term of this agreement.  Owner may, in lieu of collection of a security deposit, place a hold on a credit card in the same amount.  In the event of damage to the Rental Vehicle, Owner will apply this Security Deposit to defray the costs of necessary repairs or replacement.  If the cost for repair or replacement of damage to the Rental Vehicle exceeds the amount of the Security Deposit, Renter will be responsible for payment to the Owner of the balance of this cost. <br>
</p>

\t<h3>7. INSURANCE</h3>
<p>Renter may purchase insurance from the company or use their own insurance. Paid insurance consists of:<br>

<strong>Basic – 49.95 CAD </strong> – 500 CAD deductible for damages under 1000 CAD.

<strong>Premium – 249.95 CAD </strong>– 2500 CAD deductible for damages under 5000 CAD.

<strong>Extra – 499.95 CAD </strong>– 5000 CAD deductible for damages under 10000 CAD. 

If Renter decides to use their own insurance, they must provide to Owner with proof of insurance that would cover damage to the Rental Vehicle at the time this Car Rental Agreement is signed, as well as personal injury to the Renter, passengers in the Rented Vehicle, and other persons or property.  If the Rental Vehicle is damaged or destroyed while it is in the possession of Renter, Renter agrees to pay any required insurance deductible and also assign all rights to collect insurance proceeds to Owner.<br>
</p>

\t<h3>8. INDEMNIFICATION</h3>
<p>Renter agrees to indemnify, defend, and hold harmless the Owner for any loss, damage, or legal actions against Owner as a result of Renter’s operation or use of the Rented Vehicle during the term of this Car Rental Agreement.  This includes any attorney fees necessarily incurred for these purposes.  Renter will also pay for any parking tickets, moving violations, or other citations received while in possession of the Rented Vehicle. <br>
</p>

\t<h3>9. REPRESENTATIONS AND WARRANTIES</h3>
<p>Owner represents and warrants that to Owner’s knowledge, the Rental Vehicle is in good condition and is safe for ordinary operation of the vehicle.<br>
Renter represents and warrants that Renter is legally entitled to operate a motor vehicle under the laws of this jurisdiction and will not operate it in violation of any laws, or in any negligent or illegal manner.<br>
Renter has been given an opportunity to examine the Rental Vehicle in advance of taking possession of it, and upon such inspection, is not aware of any damage existing on the vehicle other than that notated by separate Existing Damage document.
<br>
</p>

\t<h3>10. JURISDICTION AND VENUE</h3>
<p>In the event of any dispute over this agreement, this Car Rental Agreement will be interpreted by the laws of the Province of Quebec, and any lawsuit or arbitration must be brought in the Province of Quebec.  If any portion of this agreement is found to be unenforceable by a court of competent jurisdiction, the remainder of the agreement would still have full force and effect.
<br>
</p>

\t<h3>11. ENTIRE AGREEMENT</h3>
<p>This Car Rental Agreement constitutes the entire agreement between the Parties with respect to this rental arrangement.  No modification to this agreement can be made unless in writing signed by both Parties.  Any notice required to be given to the other party will be made to the contact information below.
<br>
</p>

      <h3>12. EXTRAS</h3>
      <p><strong>Post trip cleaning:</strong> Return the car hassle free, without worrying about cleaning up after your trip. Does not cover upholstery cleaning, spills, stains, pet hair, or smoke removal. <strong>65 CAD</strong> per trip.<br></p>
      
      <input type=\"checkbox\" name=\"postTripCleaning\" value=\"1\">I want a post trip cleaning<br><br>
      <p><strong>Prepaid refuel:</strong> Save time and make drop-off a breeze by adding this Extra, which allows you to return my car at any fuel level. Price includes up to a full tank of gas. <strong>130CAD</strong> per trip.</p>
        <input type=\"checkbox\" name=\"prepaidFuel\" value=\"1\">I want a prepaid fuel option<br><br>
        Insurance policy:
                                                            <select name=\"insuranceType\">
                                                                <option value=\"\"></option>
                                                                <option value=\"Basic\" {% if v.insuranceType == 'Basic' %} selected {% endif %}>Basic plan (CAD \$49.95)</option>
                                                                <option value=\"Premium\" {% if v.insuranceType == 'Premium' %} selected {% endif %}>Premium plan (CAD \$249.95)</option>
                                                                <option value=\"Extra\" {% if v.insuranceType == 'Extra' %} selected {% endif %}>Extra plan (CAD \$499.95)</option>
                                                            </select>
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['insuranceType']}}
                                                          {% endif %}
                                                      </span><br><br>


                                                <input type=\"checkbox\" name=\"acceptContract\" value=\"accept\"><strong>I fully understand the content of this rental agreement and accepts all terms and conditions of this rental agreement.</strong>
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['acceptContract']}}
                                                          {% endif %}
                                                      </span><br><br>
    <input type=\"submit\" value=\"Set up rental agreement\" class=\"nextBtn\">
</form>
    
       

{% endblock content %}
", "step4.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\step4.html.twig");
    }
}
