<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* signup.html.twig */
class __TwigTemplate_bbe99c1f2d33e88d2507fe5488aba9903955e5ee9d6d75e2931289e586b21c37 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'menu' => [$this, 'block_menu'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "signup.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headAdd($context, array $blocks = [])
    {
        // line 3
        echo "        <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load(\"/isemailregistered/\" + email);
                });
            });
        </script>
";
    }

    // line 12
    public function block_menu($context, array $blocks = [])
    {
        // line 13
        echo "    ";
        if (((isset($context["user"]) ? $context["user"] : null) == null)) {
            echo "<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
    ";
        } else {
            // line 14
            echo "<li>You are logged with ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", []), "html", null, true);
            echo "</li><li><a data-scroll href=\"logout\">Logout</a></li>
    ";
        }
        // line 16
        echo "    ";
    }

    // line 17
    public function block_content($context, array $blocks = [])
    {
        // line 18
        echo "    <div class=\"centerContent\">
    ";
        // line 19
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 20
            echo "
            <p class=\"erromessage\">Invalid login credentials, try again or <a href=\"/register\">register</a></p>

        ";
        }
        // line 24
        echo "    </div>
    <div class=\"centerContent\">
        

        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
            <form id=\"contact-form\" method=\"post\" role=\"form\">

                <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      ";
        // line 34
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 35
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "email", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 37
        echo "                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password\"  type=\"password\" name=\"pass1\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 43
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 44
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "pass1", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 46
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password(repeated)\" type=\"password\" name=\"pass2\" size=\"50\" >
                                        <span class=\"erromessage\">
                                                      ";
        // line 51
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 52
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "pass2", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 54
        echo "                                                      </span>
\t\t\t\t\t</div>
                <div >\t\t\t\t\t\t
                    <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Sign up\">\t\t\t\t\t
                </div>
            </form>





        </div>
    ";
    }

    public function getTemplateName()
    {
        return "signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 54,  137 => 52,  135 => 51,  128 => 46,  122 => 44,  120 => 43,  112 => 37,  106 => 35,  104 => 34,  99 => 32,  89 => 24,  83 => 20,  81 => 19,  78 => 18,  75 => 17,  71 => 16,  65 => 14,  59 => 13,  56 => 12,  44 => 3,  41 => 2,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block headAdd %}
        <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load(\"/isemailregistered/\" + email);
                });
            });
        </script>
{% endblock headAdd %}
{% block menu %}
    {% if user==null %}<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
    {% else %}<li>You are logged with {{user.email}}</li><li><a data-scroll href=\"logout\">Logout</a></li>
    {% endif %}
    {% endblock menu %}
{% block content %}
    <div class=\"centerContent\">
    {% if error %}

            <p class=\"erromessage\">Invalid login credentials, try again or <a href=\"/register\">register</a></p>

        {% endif %}
    </div>
    <div class=\"centerContent\">
        

        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
            <form id=\"contact-form\" method=\"post\" role=\"form\">

                <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"{{v.email}}\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['email']}}
                                                          {% endif %}
                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password\"  type=\"password\" name=\"pass1\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.pass1}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password(repeated)\" type=\"password\" name=\"pass2\" size=\"50\" >
                                        <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.pass2}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                <div >\t\t\t\t\t\t
                    <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Sign up\">\t\t\t\t\t
                </div>
            </form>





        </div>
    {% endblock content %}
", "signup.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\signup.html.twig");
    }
}
