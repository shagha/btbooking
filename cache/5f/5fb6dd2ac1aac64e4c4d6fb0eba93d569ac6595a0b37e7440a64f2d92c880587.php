<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* calendar.html.twig */
class __TwigTemplate_5204487473cc9a39ec2799031aaad8405fa7c3c5a703676846fa890cf05519e1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'menu' => [$this, 'block_menu'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "calendar.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headAdd($context, array $blocks = [])
    {
        // line 3
        echo "        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>Calendar
        </title>
        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">

        <!-- FullCalendar -->
        <link href='css/fullcalendar.css' rel='stylesheet' />
        <link rel=\"shortcut icon\" href=\"#\" />
        
        <!-- Custom CSS -->
        <style>
           
            #calendar {
                max-width: 800px;
            }
            .col-centered{
                float: none;
                margin: 0 auto;
            }
        </style>


";
    }

    // line 28
    public function block_menu($context, array $blocks = [])
    {
        // line 29
        echo "    ";
        if (((isset($context["user"]) ? $context["user"] : null) == null)) {
            echo "<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
    ";
        } else {
            // line 30
            echo "<li>You are logged with ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", []), "html", null, true);
            echo "</li><li><a data-scroll href=\"logout\">Logout</a></li>
    ";
        }
        // line 32
        echo "    ";
    }

    // line 33
    public function block_content($context, array $blocks = [])
    {
        // line 34
        echo "   
    
    <div class=\"centerContent\">
       ";
        // line 37
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "isAdmin", [])) {
            echo " 
  <div class=\"container\">

            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">

                    <div id=\"calendar\" class=\"col-centered\">

                    </div>
                </div>

            </div>


        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.1 -->
        <script src=\"js/jquery.js\"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src=\"js/bootstrap.min.js\"></script>

        <!-- FullCalendar -->

        <script src='/js/moment.min.js'></script>
        <script src='/js/fullcalendar.min.js'></script>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

        <script>


            //
         
            function refreshTodoList() {
                \$.ajax({
                    url: '/project.php/calendrierF',
                    type: 'GET',
                    dataType: 'json'
                }).done(function (carAvailbility) {

                    \$('#calendar').fullCalendar({

                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,basicWeek,basicDay'
                        },
                        defaultDate: new Date(),
                        editable: true,
                        eventLimit: true, // allow \"more\" link when too many events
                        selectable: false,
                        selectHelper: true,
                        events: carAvailbility
                    });


                });
            }
               \$(document).ready(function () {

                refreshTodoList();
               
            });

        </script>

        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/moment.min.js'></script>
        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery.min.js'></script>
        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery-ui.min.js'></script>
        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.js'></script>

       

";
        } else {
            // line 112
            echo "<p class=\"centerContent\">You do not have access</p>
";
        }
        // line 114
        echo "

    </div>
    ";
    }

    public function getTemplateName()
    {
        return "calendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 114,  177 => 112,  99 => 37,  94 => 34,  91 => 33,  87 => 32,  81 => 30,  75 => 29,  72 => 28,  44 => 3,  41 => 2,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block headAdd %}
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>Calendar
        </title>
        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">

        <!-- FullCalendar -->
        <link href='css/fullcalendar.css' rel='stylesheet' />
        <link rel=\"shortcut icon\" href=\"#\" />
        
        <!-- Custom CSS -->
        <style>
           
            #calendar {
                max-width: 800px;
            }
            .col-centered{
                float: none;
                margin: 0 auto;
            }
        </style>


{% endblock headAdd %}
{% block menu %}
    {% if user==null %}<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
    {% else %}<li>You are logged with {{user.email}}</li><li><a data-scroll href=\"logout\">Logout</a></li>
    {% endif %}
    {% endblock menu %}
{% block content %}
   
    
    <div class=\"centerContent\">
       {% if user.isAdmin %} 
  <div class=\"container\">

            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">

                    <div id=\"calendar\" class=\"col-centered\">

                    </div>
                </div>

            </div>


        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.1 -->
        <script src=\"js/jquery.js\"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src=\"js/bootstrap.min.js\"></script>

        <!-- FullCalendar -->

        <script src='/js/moment.min.js'></script>
        <script src='/js/fullcalendar.min.js'></script>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

        <script>


            //
         
            function refreshTodoList() {
                \$.ajax({
                    url: '/project.php/calendrierF',
                    type: 'GET',
                    dataType: 'json'
                }).done(function (carAvailbility) {

                    \$('#calendar').fullCalendar({

                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,basicWeek,basicDay'
                        },
                        defaultDate: new Date(),
                        editable: true,
                        eventLimit: true, // allow \"more\" link when too many events
                        selectable: false,
                        selectHelper: true,
                        events: carAvailbility
                    });


                });
            }
               \$(document).ready(function () {

                refreshTodoList();
               
            });

        </script>

        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/moment.min.js'></script>
        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery.min.js'></script>
        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery-ui.min.js'></script>
        <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.js'></script>

       

{% else %}
<p class=\"centerContent\">You do not have access</p>
{% endif %}


    </div>
    {% endblock content %}
", "calendar.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\calendar.html.twig");
    }
}
