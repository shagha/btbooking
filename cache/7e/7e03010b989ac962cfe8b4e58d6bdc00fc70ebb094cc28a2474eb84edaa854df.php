<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* signup_success.html.twig */
class __TwigTemplate_6b0fd57a43eb3d1023076d677a0fbea78a118c4d92967f82104d73cbdc2b1a70 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'menu' => [$this, 'block_menu'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "signup_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_menu($context, array $blocks = [])
    {
        // line 3
        echo "    ";
        if (((isset($context["user"]) ? $context["user"] : null) == null)) {
            echo "<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
    ";
        } else {
            // line 4
            echo "<li>You are logged with ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", []), "html", null, true);
            echo "</li><li><a data-scroll href=\"logout\">Logout</a></li>
    ";
        }
        // line 6
        echo "    ";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        // line 8
        echo "    <div class=\"centerContent\">   
            <p>You signed up successfully  Click here to continue  <a href=\"/\"> Continue</a></p>
    </div>
    ";
    }

    public function getTemplateName()
    {
        return "signup_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 8,  59 => 7,  55 => 6,  49 => 4,  43 => 3,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block menu %}
    {% if user==null %}<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
    {% else %}<li>You are logged with {{user.email}}</li><li><a data-scroll href=\"logout\">Logout</a></li>
    {% endif %}
    {% endblock menu %}
{% block content %}
    <div class=\"centerContent\">   
            <p>You signed up successfully  Click here to continue  <a href=\"/\"> Continue</a></p>
    </div>
    {% endblock content %}
", "signup_success.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\signup_success.html.twig");
    }
}
