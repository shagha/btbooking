<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_a0f849faf776173965b4a8473ad315432930cf33b598601edbb3aefe2960d99b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headAdd($context, array $blocks = [])
    {
        // line 3
        echo "        <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load(\"/isemailregistered/\" + email);
                });
            });
        </script>
";
    }

    // line 12
    public function block_content($context, array $blocks = [])
    {
        // line 13
        echo "      ";
        if (($context["errorList"] ?? null)) {
            // line 14
            echo "        <ul>
            ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 16
                echo "                <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "fName", []), "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "        </ul>
    ";
        }
        // line 20
        echo "    

    <div class=\"centerContent \">
         
        
        
        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "fName", []), "html", null, true);
        echo "\" placeholder=\"First name\" type=\"text\" name=\"fName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      ";
        // line 32
        if (($context["errorList"] ?? null)) {
            // line 33
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "fName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 35
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "lName", []), "html", null, true);
        echo "\" placeholder=\"Last name\" type=\"text\" name=\"lName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 41
        if (($context["errorList"] ?? null)) {
            // line 42
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "lName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 44
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "driverLicense", []), "html", null, true);
        echo "\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 50
        if (($context["errorList"] ?? null)) {
            // line 51
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "driverLicense", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 53
        echo "                                                      </span>
\t\t\t\t\t</div>
                                                      <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "insurancePolicy", []), "html", null, true);
        echo "\" placeholder=\"Insurance policy\" type=\"text\" name=\"insurancePolicy\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 58
        if (($context["errorList"] ?? null)) {
            // line 59
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "insurancePolicy", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 61
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "address", []), "html", null, true);
        echo "\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 67
        if (($context["errorList"] ?? null)) {
            // line 68
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "address", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 70
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "city", []), "html", null, true);
        echo "\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 76
        if (($context["errorList"] ?? null)) {
            // line 77
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "city", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 79
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "province", []), "html", null, true);
        echo "\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 84
        if (($context["errorList"] ?? null)) {
            // line 85
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "province", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 87
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "country", []), "html", null, true);
        echo "\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 92
        if (($context["errorList"] ?? null)) {
            // line 93
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "country", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 95
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "postalCode", []), "html", null, true);
        echo "\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 100
        if (($context["errorList"] ?? null)) {
            // line 101
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "postalCode", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 103
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "phoneNumber", []), "html", null, true);
        echo "\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 108
        if (($context["errorList"] ?? null)) {
            // line 109
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "phoneNumber", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 111
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", []), "html", null, true);
        echo "\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      ";
        // line 116
        if (($context["errorList"] ?? null)) {
            // line 117
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "email", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 119
        echo "                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password\"  type=\"password\" name=\"pass1\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 125
        if (($context["errorList"] ?? null)) {
            // line 126
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "pass1", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 128
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password(repeated)\" type=\"password\" name=\"pass2\" size=\"50\" >
                                        <span class=\"erromessage\">
                                                      ";
        // line 133
        if (($context["errorList"] ?? null)) {
            // line 134
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "pass2", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 136
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" ";
        // line 139
        echo (($this->getAttribute(($context["v"] ?? null), "mailerConsent", [])) ? ("checked") : (""));
        echo "><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div>
                                    
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Register\">
\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t</form>
\t\t
        
        
        
        
       
    </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  326 => 139,  321 => 136,  315 => 134,  313 => 133,  306 => 128,  300 => 126,  298 => 125,  290 => 119,  284 => 117,  282 => 116,  277 => 114,  272 => 111,  266 => 109,  264 => 108,  259 => 106,  254 => 103,  248 => 101,  246 => 100,  241 => 98,  236 => 95,  230 => 93,  228 => 92,  223 => 90,  218 => 87,  212 => 85,  210 => 84,  205 => 82,  200 => 79,  194 => 77,  192 => 76,  187 => 74,  181 => 70,  175 => 68,  173 => 67,  168 => 65,  162 => 61,  156 => 59,  154 => 58,  149 => 56,  144 => 53,  138 => 51,  136 => 50,  131 => 48,  125 => 44,  119 => 42,  117 => 41,  112 => 39,  106 => 35,  100 => 33,  98 => 32,  93 => 30,  81 => 20,  77 => 18,  68 => 16,  64 => 15,  61 => 14,  58 => 13,  55 => 12,  43 => 3,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block headAdd %}
        <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load(\"/isemailregistered/\" + email);
                });
            });
        </script>
{% endblock headAdd %}
{% block content %}
      {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error.fName}}</li>
            {% endfor %}
        </ul>
    {% endif %}
    

    <div class=\"centerContent \">
         
        
        
        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"{{v.fName}}\" placeholder=\"First name\" type=\"text\" name=\"fName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.fName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.lName}}\" placeholder=\"Last name\" type=\"text\" name=\"lName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.lName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.driverLicense}}\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.driverLicense}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                                      <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.insurancePolicy}}\" placeholder=\"Insurance policy\" type=\"text\" name=\"insurancePolicy\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.insurancePolicy}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.address}}\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.address}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.city}}\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.city}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.province}}\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.province}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.country}}\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.country}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.postalCode}}\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.postalCode}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.phoneNumber}}\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.phoneNumber}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"{{v.email}}\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['email']}}
                                                          {% endif %}
                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password\"  type=\"password\" name=\"pass1\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.pass1}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Password(repeated)\" type=\"password\" name=\"pass2\" size=\"50\" >
                                        <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.pass2}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" {{v.mailerConsent ? 'checked' : ''}}><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div>
                                    
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Register\">
\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t</form>
\t\t
        
        
        
        
       
    </div>
    </div>
{% endblock content %}
", "register.html.twig", "C:\\xampp\\htdocs\\project\\templates\\register.html.twig");
    }
}
