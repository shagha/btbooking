<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contract_edit.html.twig */
class __TwigTemplate_cf984140d0d632b5113a7b6f0b17d4db06fd642024811aef3a4e2ed8b6039524 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "contract_edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    
    <div class=\"centerContent \">

\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "userFName", []), "html", null, true);
        echo "\" placeholder=\"First name\" type=\"text\" name=\"userFName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      ";
        // line 12
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 13
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "fName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 15
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "userLName", []), "html", null, true);
        echo "\" placeholder=\"Last name\" type=\"text\" name=\"userLName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 21
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 22
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "lName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 24
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "driverLicense", []), "html", null, true);
        echo "\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 30
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 31
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "driverLicense", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 33
        echo "                                                      </span>
                                        </div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "address", []), "html", null, true);
        echo "\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 39
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 40
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "address", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 42
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "city", []), "html", null, true);
        echo "\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 48
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 49
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "city", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 51
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "province", []), "html", null, true);
        echo "\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 56
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 57
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "province", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 59
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "country", []), "html", null, true);
        echo "\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 64
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 65
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "country", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 67
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "postalCode", []), "html", null, true);
        echo "\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 72
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 73
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "postalCode", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 75
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "phoneNumber", []), "html", null, true);
        echo "\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 80
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 81
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "phoneNumber", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 83
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      ";
        // line 88
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 89
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "email", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 91
        echo "                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" ";
        // line 96
        echo (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "mailerConsent", [])) ? ("checked") : (""));
        echo "><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div> 
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Next\">
                                        </div>\t\t\t\t\t
\t\t\t\t</form>
\t\t

       </div>
";
    }

    public function getTemplateName()
    {
        return "contract_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 96,  229 => 91,  223 => 89,  221 => 88,  216 => 86,  211 => 83,  205 => 81,  203 => 80,  198 => 78,  193 => 75,  187 => 73,  185 => 72,  180 => 70,  175 => 67,  169 => 65,  167 => 64,  162 => 62,  157 => 59,  151 => 57,  149 => 56,  144 => 54,  139 => 51,  133 => 49,  131 => 48,  126 => 46,  120 => 42,  114 => 40,  112 => 39,  107 => 37,  101 => 33,  95 => 31,  93 => 30,  88 => 28,  82 => 24,  76 => 22,  74 => 21,  69 => 19,  63 => 15,  57 => 13,  55 => 12,  50 => 10,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    
    <div class=\"centerContent \">

\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"{{v.userFName}}\" placeholder=\"First name\" type=\"text\" name=\"userFName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.fName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.userLName}}\" placeholder=\"Last name\" type=\"text\" name=\"userLName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.lName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.driverLicense}}\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.driverLicense}}
                                                          {% endif %}
                                                      </span>
                                        </div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.address}}\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.address}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.city}}\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.city}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.province}}\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.province}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.country}}\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.country}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.postalCode}}\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.postalCode}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.phoneNumber}}\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.phoneNumber}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"{{v.email}}\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['email']}}
                                                          {% endif %}
                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" {{v.mailerConsent ? 'checked' : ''}}><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div> 
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Next\">
                                        </div>\t\t\t\t\t
\t\t\t\t</form>
\t\t

       </div>
{% endblock content %}", "contract_edit.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\contract_edit.html.twig");
    }
}
