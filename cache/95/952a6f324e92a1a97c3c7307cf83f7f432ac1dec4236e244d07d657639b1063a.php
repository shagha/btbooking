<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* step1.html.twig */
class __TwigTemplate_7ce1a00a023b57517c3a89dc2264eeaed36e92e0a70be20e3b551e36831bfe82 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "step1.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <form class=\"centerContent\" method='post'>
            <div id=\"step-1\">
                <p>Choose the car:</p>
                ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["carsList"]) ? $context["carsList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["auto"]) {
            // line 8
            echo "                    <p class=\"carsMini\">
                        <input type=\"radio\" name=\"carId\" value=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "id", []), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($context["auto"], "id", []) == (isset($context["selectedId"]) ? $context["selectedId"] : null))) {
                echo " checked ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "make", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "model", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "year", []), "html", null, true);
            echo "<br>
                        <img src=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "pathPhoto", []), "html", null, true);
            echo "\" height=\"100\"><br>
                    </p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "                <br><span class=\"erromessage\">
                        ";
        // line 14
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 15
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "carId", []), "html", null, true);
            echo "
                        ";
        }
        // line 17
        echo "                </span><br>
                    
                 <p class=\"datepickers\">Pick up date: <input type=\"date\" id=\"pickUp\" name=\"pickUpDate\"  min=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "pickUpDate", []), "html", null, true);
        echo "\" required></p>
                    
                <p class=\"datepickers\"> Pick up time: <select name=\"pickUpTime\" id=\"pickUpTime\">
                    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(8, 22));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 23
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo ":00</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        
                    </select>
                    
                <p class=\"datepickers\">Return date: <input type=\"date\" id=\"return\" name=\"returnDate\" min=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["tomorrow"]) ? $context["tomorrow"] : null), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "returnDate", []), "html", null, true);
        echo "\" required></p>
                <br><span class=\"erromessage\">
                        ";
        // line 29
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 30
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "dates", []), "html", null, true);
            echo "
                        ";
        }
        // line 32
        echo "                </span><br>
                <input type=\"submit\" value=\"Next\" class=\"nextBtn\">
            </div>
</form>
                        
                        
";
    }

    public function getTemplateName()
    {
        return "step1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 32,  129 => 30,  127 => 29,  120 => 27,  115 => 24,  104 => 23,  100 => 22,  92 => 19,  88 => 17,  82 => 15,  80 => 14,  77 => 13,  68 => 10,  54 => 9,  51 => 8,  47 => 7,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    <form class=\"centerContent\" method='post'>
            <div id=\"step-1\">
                <p>Choose the car:</p>
                {% for auto in carsList %}
                    <p class=\"carsMini\">
                        <input type=\"radio\" name=\"carId\" value=\"{{auto.id}}\" {% if auto.id==selectedId %} checked {% endif %}>{{auto.make}} {{auto.model}} {{auto.year}}<br>
                        <img src=\"{{auto.pathPhoto}}\" height=\"100\"><br>
                    </p>
                {% endfor %}
                <br><span class=\"erromessage\">
                        {% if errorList %}
                            {{errorList.carId}}
                        {% endif %}
                </span><br>
                    
                 <p class=\"datepickers\">Pick up date: <input type=\"date\" id=\"pickUp\" name=\"pickUpDate\"  min=\"{{today}}\" value=\"{{v.pickUpDate}}\" required></p>
                    
                <p class=\"datepickers\"> Pick up time: <select name=\"pickUpTime\" id=\"pickUpTime\">
                    {% for i in 8..22 %}
                        <option value=\"{{i}}\">{{i}}:00</option>
                    {% endfor %}        
                    </select>
                    
                <p class=\"datepickers\">Return date: <input type=\"date\" id=\"return\" name=\"returnDate\" min=\"{{tomorrow}}\" value=\"{{v.returnDate}}\" required></p>
                <br><span class=\"erromessage\">
                        {% if errorList %}
                            {{errorList.dates}}
                        {% endif %}
                </span><br>
                <input type=\"submit\" value=\"Next\" class=\"nextBtn\">
            </div>
</form>
                        
                        
{% endblock content %}", "step1.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\step1.html.twig");
    }
}
