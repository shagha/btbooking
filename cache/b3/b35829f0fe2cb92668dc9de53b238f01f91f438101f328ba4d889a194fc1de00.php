<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* step3.html.twig */
class __TwigTemplate_b78e267b5c80db282b047877155bb379f1298554b3c8260aa4b1438d0c1ebb5a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "step3.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    
    <div class=\"centerContent \">
         
        
        
        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "userFName", []), "html", null, true);
        echo "\" placeholder=\"First name\" type=\"text\" name=\"userFName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      ";
        // line 15
        if (($context["errorList"] ?? null)) {
            // line 16
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "fName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 18
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "userLName", []), "html", null, true);
        echo "\" placeholder=\"Last name\" type=\"text\" name=\"userLName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 24
        if (($context["errorList"] ?? null)) {
            // line 25
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "lName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 27
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "driverLicense", []), "html", null, true);
        echo "\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 33
        if (($context["errorList"] ?? null)) {
            // line 34
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "driverLicense", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 36
        echo "                                                      </span>
                                        </div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "address", []), "html", null, true);
        echo "\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 42
        if (($context["errorList"] ?? null)) {
            // line 43
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "address", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 45
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "city", []), "html", null, true);
        echo "\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 51
        if (($context["errorList"] ?? null)) {
            // line 52
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "city", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 54
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "province", []), "html", null, true);
        echo "\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 59
        if (($context["errorList"] ?? null)) {
            // line 60
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "province", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 62
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "country", []), "html", null, true);
        echo "\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 67
        if (($context["errorList"] ?? null)) {
            // line 68
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "country", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 70
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "postalCode", []), "html", null, true);
        echo "\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 75
        if (($context["errorList"] ?? null)) {
            // line 76
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "postalCode", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 78
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "phoneNumber", []), "html", null, true);
        echo "\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 83
        if (($context["errorList"] ?? null)) {
            // line 84
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "phoneNumber", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 86
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", []), "html", null, true);
        echo "\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      ";
        // line 91
        if (($context["errorList"] ?? null)) {
            // line 92
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["errorList"] ?? null), "email", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 94
        echo "                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" ";
        // line 99
        echo (($this->getAttribute(($context["v"] ?? null), "mailerConsent", [])) ? ("checked") : (""));
        echo "><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div> 
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Next\">
                                        </div>\t\t\t\t\t
\t\t\t\t</form>
\t\t

    </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "step3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 99,  232 => 94,  226 => 92,  224 => 91,  219 => 89,  214 => 86,  208 => 84,  206 => 83,  201 => 81,  196 => 78,  190 => 76,  188 => 75,  183 => 73,  178 => 70,  172 => 68,  170 => 67,  165 => 65,  160 => 62,  154 => 60,  152 => 59,  147 => 57,  142 => 54,  136 => 52,  134 => 51,  129 => 49,  123 => 45,  117 => 43,  115 => 42,  110 => 40,  104 => 36,  98 => 34,  96 => 33,  91 => 31,  85 => 27,  79 => 25,  77 => 24,  72 => 22,  66 => 18,  60 => 16,  58 => 15,  53 => 13,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    
    <div class=\"centerContent \">
         
        
        
        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"{{v.userFName}}\" placeholder=\"First name\" type=\"text\" name=\"userFName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.fName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.userLName}}\" placeholder=\"Last name\" type=\"text\" name=\"userLName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.lName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.driverLicense}}\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.driverLicense}}
                                                          {% endif %}
                                                      </span>
                                        </div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.address}}\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.address}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.city}}\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.city}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.province}}\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.province}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.country}}\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.country}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.postalCode}}\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.postalCode}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.phoneNumber}}\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.phoneNumber}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"{{v.email}}\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['email']}}
                                                          {% endif %}
                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" {{v.mailerConsent ? 'checked' : ''}}><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div> 
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Next\">
                                        </div>\t\t\t\t\t
\t\t\t\t</form>
\t\t

    </div>
    </div>
{% endblock content %}
", "step3.html.twig", "C:\\xampp\\htdocs\\project\\templates\\step3.html.twig");
    }
}
