<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* step2.html.twig */
class __TwigTemplate_f9b083767bb3232f00377f75cca4694f17c540bc38dcfff97023d74bf4e5290e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "step2.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <h2 class=\"steps\">Step 2</h2>
    <form class=\"centerContent\" method='post'>
            <div id=\"stepDiv\">
                <p>Sorry, but the car you have selected is not available during the time period chosen. Please, choose one of the following available during requested time cars or<a href=\"/ipd17-project/booking\"> click here</a> to change the rental dates.</p>
                <p>Choose the car:</p>
                ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["availableCarsList"]) ? $context["availableCarsList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["auto"]) {
            // line 10
            echo "                    <p class=\"carsMini\">
                        <input type=\"radio\" name=\"newCarId\" value=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "id", []), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($context["auto"], "id", []) == (isset($context["selectedId"]) ? $context["selectedId"] : null))) {
                echo " checked ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "make", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "model", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "year", []), "html", null, true);
            echo "<br>
                        <img src=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "pathPhoto", []), "html", null, true);
            echo "\" height=\"100\"><br>
                    </p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "                <br><span class=\"erromessage\">
                        ";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "
                </span><br>
                
                <input type=\"submit\" value=\"Next\" class=\"nextBtn\">
            </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "step2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 16,  79 => 15,  70 => 12,  56 => 11,  53 => 10,  49 => 9,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    <h2 class=\"steps\">Step 2</h2>
    <form class=\"centerContent\" method='post'>
            <div id=\"stepDiv\">
                <p>Sorry, but the car you have selected is not available during the time period chosen. Please, choose one of the following available during requested time cars or<a href=\"/ipd17-project/booking\"> click here</a> to change the rental dates.</p>
                <p>Choose the car:</p>
                {% for auto in availableCarsList %}
                    <p class=\"carsMini\">
                        <input type=\"radio\" name=\"newCarId\" value=\"{{auto.id}}\" {% if auto.id==selectedId %} checked {% endif %}>{{auto.make}} {{auto.model}} {{auto.year}}<br>
                        <img src=\"{{auto.pathPhoto}}\" height=\"100\"><br>
                    </p>
                {% endfor %}
                <br><span class=\"erromessage\">
                        {{error}}
                </span><br>
                
                <input type=\"submit\" value=\"Next\" class=\"nextBtn\">
            </div>
</form>
{% endblock content %}", "step2.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\step2.html.twig");
    }
}
