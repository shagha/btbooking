<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* manage_contracts.html.twig */
class __TwigTemplate_64fc1ae754c678563d14431bee92219873df295def1ed845b93358362786fd69 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "manage_contracts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "<div class=\"centerContent\">
    ";
        // line 5
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "isAdmin", [])) {
            // line 6
            echo "     
        <div>
            <p class=\"centerText\"><a href=\"/ipd17-project/booking\">Create a new contract</a></p><br><br>

                      <table align=\"center\">
                          <thead>
                              <tr>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Car</th>
                                  <th>From</th>
                                  <th>To</th>
                                  <th>Returned</th>
                                  <th>Start km</th>
                                  <th>Return km</th>
                                  <th>Insurance</th>
                                  <th>Cleaning</th>
                                  <th>Fueling</th>
                                  <th>Allowed outside</th>
                              </tr>
                          </thead>
                          <tbody>
                              ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contractsList"]) ? $context["contractsList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["contract"]) {
                // line 29
                echo "                                  <tr>
                                      <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "userFName", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "userLName", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "make", []), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "model", []), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "year", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "startDate", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "dueDate", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "returnDate", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "milageAtStart", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "milageAtEnd", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "insuranceType", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "postTripCleaning", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "prepaidFuel", []), "html", null, true);
                echo "</td>
                                      <td>";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "AllowedOutSideQC", []), "html", null, true);
                echo "</td>
                                      <td><button onclick=\"window.location='contracts/edit/";
                // line 42
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "id", []), "html", null, true);
                echo "';\">Edit</button>
                                          <button onclick=\"window.location='contracts/delete/";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["contract"], "id", []), "html", null, true);
                echo "';\">Delete</button></td>
                                  </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contract'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "    

                          </tbody>
                      </table><br><br>
                  </div>

    ";
        } else {
            // line 52
            echo "<p class=\"centerContent\">You do not have access</p>

";
        }
        // line 55
        echo "    
</div>
";
    }

    public function getTemplateName()
    {
        return "manage_contracts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 55,  152 => 52,  143 => 45,  134 => 43,  130 => 42,  126 => 41,  122 => 40,  118 => 39,  114 => 38,  110 => 37,  106 => 36,  102 => 35,  98 => 34,  94 => 33,  86 => 32,  82 => 31,  78 => 30,  75 => 29,  71 => 28,  47 => 6,  45 => 5,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
<div class=\"centerContent\">
    {% if user.isAdmin %}
     
        <div>
            <p class=\"centerText\"><a href=\"/ipd17-project/booking\">Create a new contract</a></p><br><br>

                      <table align=\"center\">
                          <thead>
                              <tr>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Car</th>
                                  <th>From</th>
                                  <th>To</th>
                                  <th>Returned</th>
                                  <th>Start km</th>
                                  <th>Return km</th>
                                  <th>Insurance</th>
                                  <th>Cleaning</th>
                                  <th>Fueling</th>
                                  <th>Allowed outside</th>
                              </tr>
                          </thead>
                          <tbody>
                              {% for contract in contractsList %}
                                  <tr>
                                      <td>{{contract.userFName}}</td>
                                      <td>{{contract.userLName}}</td>
                                      <td>{{contract.make}} {{contract.model}} {{contract.year}}</td>
                                      <td>{{contract.startDate}}</td>
                                      <td>{{contract.dueDate}}</td>
                                      <td>{{contract.returnDate}}</td>
                                      <td>{{contract.milageAtStart}}</td>
                                      <td>{{contract.milageAtEnd}}</td>
                                      <td>{{contract.insuranceType}}</td>
                                      <td>{{contract.postTripCleaning}}</td>
                                      <td>{{contract.prepaidFuel}}</td>
                                      <td>{{contract.AllowedOutSideQC}}</td>
                                      <td><button onclick=\"window.location='contracts/edit/{{contract.id}}';\">Edit</button>
                                          <button onclick=\"window.location='contracts/delete/{{contract.id}}';\">Delete</button></td>
                                  </tr>
                            {% endfor %}    

                          </tbody>
                      </table><br><br>
                  </div>

    {% else %}
<p class=\"centerContent\">You do not have access</p>

{% endif %}
    
</div>
{% endblock content %}

", "manage_contracts.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\manage_contracts.html.twig");
    }
}
