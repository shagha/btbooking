<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking.html.twig */
class __TwigTemplate_699361ef22117ee933d209fbdcca88c75ad913dcd633b5f4449ecdc593f3c74d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "booking.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <h2 class=\"steps\">Step 1</h2>
    <form class=\"centerContent\">
            <div id=\"step-1\">
                <p>Choose the car:</p>
                ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["carsList"]) ? $context["carsList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["auto"]) {
            // line 9
            echo "                    <p class=\"carsMini\">
                        <input type=\"radio\" name=\"car\" value=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "id", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "make", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "model", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "year", []), "html", null, true);
            echo "<br>
                        <img src=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "pathPhoto", []), "html", null, true);
            echo "\" height=\"100\"><br>
                    </p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "                <br><span class=\"errorMessage\" id=\"carNotChosen\"></span><br>
                    
                 <p class=\"datepickers\">Pick up date: <input type=\"date\" id=\"pickUp\" name=\"pickUpDate\"  min=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" required></p>
                    
                <p class=\"datepickers\"> Pick up time: <select name=\"pickUpTime\" id=\"pickUpTime\">
                    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(8, 22));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 20
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo ":00</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "        
                    </select>
                    
                <p class=\"datepickers\">Return date: <input type=\"date\" id=\"return\" name=\"returnDate\" min=\"";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["tomorrow"]) ? $context["tomorrow"] : null), "html", null, true);
        echo "\" required></p>
                <br><span class=\"errorMessage\" id=\"datesProblem\"></span><br>
                <input type=\"submit\" value=\"Next\" class=\"nextBtn\">
            </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "booking.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 24,  99 => 21,  88 => 20,  84 => 19,  78 => 16,  74 => 14,  65 => 11,  55 => 10,  52 => 9,  48 => 8,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    <h2 class=\"steps\">Step 1</h2>
    <form class=\"centerContent\">
            <div id=\"step-1\">
                <p>Choose the car:</p>
                {% for auto in carsList %}
                    <p class=\"carsMini\">
                        <input type=\"radio\" name=\"car\" value=\"{{auto.id}}\">{{auto.make}} {{auto.model}} {{auto.year}}<br>
                        <img src=\"{{auto.pathPhoto}}\" height=\"100\"><br>
                    </p>
                {% endfor %}
                <br><span class=\"errorMessage\" id=\"carNotChosen\"></span><br>
                    
                 <p class=\"datepickers\">Pick up date: <input type=\"date\" id=\"pickUp\" name=\"pickUpDate\"  min=\"{{today}}\" required></p>
                    
                <p class=\"datepickers\"> Pick up time: <select name=\"pickUpTime\" id=\"pickUpTime\">
                    {% for i in 8..22 %}
                        <option value=\"{{i}}\">{{i}}:00</option>
                    {% endfor %}        
                    </select>
                    
                <p class=\"datepickers\">Return date: <input type=\"date\" id=\"return\" name=\"returnDate\" min=\"{{tomorrow}}\" required></p>
                <br><span class=\"errorMessage\" id=\"datesProblem\"></span><br>
                <input type=\"submit\" value=\"Next\" class=\"nextBtn\">
            </div>
</form>
{% endblock content %}", "booking.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\booking.html.twig");
    }
}
