<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* charts.html.twig */
class __TwigTemplate_2e9b7112b0e7c6e3d310f8d7d564666a31bd1dcfd9e537dad5372851edd91657 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "charts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headAdd($context, array $blocks = [])
    {
        // line 3
        echo "        <script>
            \$(document).ready(function() {
     
                \$('select[name=month]').on(\"change\", function(){
                var selectedMonth = \$('#monthList option:selected').val();          
                \$('#createChart').load(\"/firstchart/\"+selectedMonth);

             
    });
                   
                    
             
            });
        </script>
";
    }

    // line 19
    public function block_content($context, array $blocks = [])
    {
        // line 20
        if ($this->getAttribute(($context["user"] ?? null), "isAdmin", [])) {
            // line 21
            echo " <div class=\"centerContent\">
    
  <select name=\"month\" id='monthList'>
 <option value=\"\" >Select a Month</option>         
  <option value=\"1\" >JAN</option>
  <option value=\"2\" >FEB</option>
  <option value=\"3\">APR</option>
  <option value=\"4\" >MAR</option>
  <option value=\"5\">MAY</option>
  <option value=\"6\">JUN</option>
  <option value=\"7\">JUL</option>
  <option value=\"8\">AUG</option>
  <option value=\"9\">SEP</option>
   <option value=\"10\">OCT</option>
   <option value=\"11\">NOV</option>
    <option value=\"12\">DEC</option>
</select>
 </div>
    <div class=\"centerContent\">
      
      
        <span id=\"createChart\"></span>
    </div>
";
        } else {
            // line 45
            echo "<p class=\"centerContent\">You do not have access</p>


";
        }
        // line 49
        echo "    

       
    ";
    }

    public function getTemplateName()
    {
        return "charts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 49,  92 => 45,  66 => 21,  64 => 20,  61 => 19,  43 => 3,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block headAdd %}
        <script>
            \$(document).ready(function() {
     
                \$('select[name=month]').on(\"change\", function(){
                var selectedMonth = \$('#monthList option:selected').val();          
                \$('#createChart').load(\"/firstchart/\"+selectedMonth);

             
    });
                   
                    
             
            });
        </script>
{% endblock headAdd %}

{% block content %}
{% if user.isAdmin %}
 <div class=\"centerContent\">
    
  <select name=\"month\" id='monthList'>
 <option value=\"\" >Select a Month</option>         
  <option value=\"1\" >JAN</option>
  <option value=\"2\" >FEB</option>
  <option value=\"3\">APR</option>
  <option value=\"4\" >MAR</option>
  <option value=\"5\">MAY</option>
  <option value=\"6\">JUN</option>
  <option value=\"7\">JUL</option>
  <option value=\"8\">AUG</option>
  <option value=\"9\">SEP</option>
   <option value=\"10\">OCT</option>
   <option value=\"11\">NOV</option>
    <option value=\"12\">DEC</option>
</select>
 </div>
    <div class=\"centerContent\">
      
      
        <span id=\"createChart\"></span>
    </div>
{% else %}
<p class=\"centerContent\">You do not have access</p>


{% endif %}
    

       
    {% endblock content %}
", "charts.html.twig", "C:\\xampp\\htdocs\\project\\templates\\charts.html.twig");
    }
}
