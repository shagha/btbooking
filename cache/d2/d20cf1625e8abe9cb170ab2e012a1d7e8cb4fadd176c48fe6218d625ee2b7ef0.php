<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_0fcd440b0546a399d6caa5e410b25b845d28b0c8295c97b20557ffc98eea1a19 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"centerContent\">
    ";
        // line 5
        if (($context["error"] ?? null)) {
            // line 6
            echo "
            <p class=\"erromessage\">Invalid login credentials, try again or <a href=\"/signup\">Sign up</a></p>

        ";
        }
        // line 10
        echo "    </div>
    
    <div class=\"centerContent\">
        

        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
            <form id=\"contact-form\" method=\"post\" role=\"form\">

                <div class=\"form-group\">
                    <input class=\"form-control\" placeholder=\"Email\" type=\"email\" name=\"email\" size=\"50\">

                </div>

                <div class=\"form-group\">
                    <input class=\"form-control\" type=\"password\" placeholder=\"Password\"  name=\"password\" size=\"50\" ><br>

                </div>\t\t\t\t\t
                <div >\t\t\t\t\t\t
                    <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Login\">\t\t\t\t\t
                </div>
            </form>





        </div>
    </div>
    ";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 10,  47 => 6,  45 => 5,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    <div class=\"centerContent\">
    {% if error %}

            <p class=\"erromessage\">Invalid login credentials, try again or <a href=\"/signup\">Sign up</a></p>

        {% endif %}
    </div>
    
    <div class=\"centerContent\">
        

        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
            <form id=\"contact-form\" method=\"post\" role=\"form\">

                <div class=\"form-group\">
                    <input class=\"form-control\" placeholder=\"Email\" type=\"email\" name=\"email\" size=\"50\">

                </div>

                <div class=\"form-group\">
                    <input class=\"form-control\" type=\"password\" placeholder=\"Password\"  name=\"password\" size=\"50\" ><br>

                </div>\t\t\t\t\t
                <div >\t\t\t\t\t\t
                    <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Login\">\t\t\t\t\t
                </div>
            </form>





        </div>
    </div>
    {% endblock content %}
", "login.html.twig", "C:\\xampp\\htdocs\\project\\templates\\login.html.twig");
    }
}
