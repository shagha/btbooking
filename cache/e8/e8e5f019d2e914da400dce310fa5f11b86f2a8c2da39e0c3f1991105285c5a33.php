<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_632be853d522cc626ed1a187808ead9d29b139d62ec9742df50d97974ce9ad35 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
            'footerAdd' => [$this, 'block_footerAdd'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-138491332-1\"></script>
        <!-- Main jQuery -->
        <script type=\"text/javascript\" src=\"plugins/jquery/dist/jquery.min.js\"></script>
        <script type=\"text/javascript\" src=\"plugins/bootstrap/dist/js/bootstrap.min.js\"></script>
       
       <!-- Slick Carousel -->
    <script type=\"text/javascript\" src=\"plugins/slick-carousel/slick/slick.min.js\"></script>
    <!-- Portfolio Filtering -->
    <script type=\"text/javascript\" src=\"plugins/mixitup/dist/mixitup.min.js\"></script>
    <!-- Smooth Scroll -->
    <script type=\"text/javascript\" src=\"plugins/smooth-scroll/dist/js/smooth-scroll.min.js\"></script>
    <!-- Magnific popup -->
    <script type=\"text/javascript\" src=\"plugins/magnific-popup/dist/jquery.magnific-popup.min.js\"></script>
    <!-- Google Map API -->
    <script type=\"text/javascript\"  src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>
    <!-- Sticky Nav -->
    <script type=\"text/javascript\" src=\"plugins/Sticky/jquery.sticky.js\"></script>
    <!-- Number Counter Script -->
    <script type=\"text/javascript\" src=\"plugins/count-to/jquery.countTo.js\"></script>
    <!-- wow.min Script -->
    <script type=\"text/javascript\" src=\"plugins/wow/dist/wow.min.js\"></script>
    <!-- Custom js -->
    <script type=\"text/javascript\" src=\"js/script.js\"></script>

\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        <meta name=\"description\" content=\"Luxury cars rental in Montreal.\">
        <meta name=\"keywords\" content=\"luxury, car, rent, rent car Montreal, rent cars, rent car, rent cars Montreal, luxury car, luxury cars, BT, BT luxury, luxury cars Montreal, luxury car Montreal, rent luxury car Montreal, rent luxury cars Montreal, BT luxury cars\">
        
        <meta name=\"author\" content=\"Mushu\">
\t\t
        <title>BT Luxury Car Rental Montreal</title>
\t\t
\t\t<!-- Mobile Specific Meta
\t\t================================================== -->
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t
\t\t
\t\t<!-- CSS
\t\t================================================== -->
\t\t<!-- Fontawesome Icon font -->
        <link rel=\"stylesheet\" href=\"plugins/themefisher-font/style.css\">
\t\t<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">
\t\t<!-- bootstrap.min css -->
        <link rel=\"stylesheet\" href=\"plugins/bootstrap/dist/css/bootstrap.min.css\">
\t\t<!-- Animate.css -->
        <link rel=\"stylesheet\" href=\"plugins/animate-css/animate.css\">
        <!-- Magnific popup css -->
        <link rel=\"stylesheet\" href=\"plugins/magnific-popup/dist/magnific-popup.css\">
\t\t<!-- Slick Carousel -->
        <link rel=\"stylesheet\" href=\"plugins/slick-carousel/slick/slick.css\">
        <link rel=\"stylesheet\" href=\"plugins/slick-carousel/slick/slick-theme.css\">
\t\t<!-- Main Stylesheet -->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/myStyle.css\">
        
        <title>";
        // line 61
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 62
        $this->displayBlock('headAdd', $context, $blocks);
        // line 63
        echo "</head>
<body id=\"body\" data-spy=\"scroll\" data-target=\".navbar\" data-offset=\"50\">

    <header id=\"navigation\" class=\"navbar navigation\">
        <div class=\"container\">
            <div class=\"navbar-header\">

                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>

                <!-- logo -->
                <span class=\"phone align-middle\"> &#9990; +1 514 516 7070</span>
                <span class=\"phone align-middle\"> &#9990; +1 514 573 7567</span>
            </div>

            <!-- main nav -->
            <nav class=\"collapse navbar-collapse navbar-right\" role=\"Navigation\">
                <ul id=\"nav\" class=\"nav navbar-nav navigation-menu\">
                    <li><a data-scroll href=\"/\">Home</a></li>
                    <li><a data-scroll href=\"/#about\">About Us</a></li>
                    <li><a data-scroll href=\"/#portfolio\">Our Cars</a></li>
                    <li><a data-scroll href=\"/#services\">Services</a></li>
                    <li><a data-scroll href=\"/#pricing\">Pricing</a></li>
                    <li><a data-scroll href=\"/#contact-us\">Contact</a></li>
                    <li><a href=\"booking\">Book</a></li>
                    ";
        // line 92
        if ((($context["user"] ?? null) == null)) {
            echo "<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
                    ";
        } else {
            // line 93
            echo "<li><a href=\"#\">Welcome back, ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "userFName", []), "html", null, true);
            echo "!</a></li><li><a data-scroll href=\"logout\">Logout</a></li>
                         ";
            // line 94
            if ($this->getAttribute(($context["user"] ?? null), "isAdmin", [])) {
                // line 95
                echo "                         <li><a data-scroll href=\"management\">Management</a></li>
            ";
            }
            // line 97
            echo "                    ";
        }
        // line 98
        echo "                </ul>
            </nav>
        </div>
    </header>


    <div>";
        // line 104
        $this->displayBlock('content', $context, $blocks);
        // line 105
        echo "       
    </div>

  
    <footer id=\"footer\" class=\"bg-one\">
        <div class=\"container\">
            <div class=\"row wow fadeInUp\" data-wow-duration=\"500ms\">
                <div class=\"col-lg-12\">

                    <!-- Footer Social Links -->
                    <div class=\"social-icon\">
                        <ul class=\"list-inline\">
                            <li><a href=\"https://www.facebook.com/BTLuxuryCars/\"><i class=\"tf-ion-social-facebook\"></i></a></li>
                            <!--<li><a href=\"#\"><i class=\"tf-ion-social-twitter\"></i></a></li>
                            <li><a href=\"#\"><i class=\"tf-ion-social-google-outline\"></i></a></li>-->
                            <li><a href=\"https://www.youtube.com/channel/UC25ppQ_5YKSzFWHR6Ge85rg\"><i class=\"tf-ion-social-youtube\"></i></a></li>
                            <li><a href=\"https://www.instagram.com/bt_luxury_cars/\"><i class=\"tf-ion-social-instagram\"></i></a></li>
                            <li><a href=\"#\"><i class=\"tf-ion-social-linkedin\"></i></a></li>
                            <!--<li><a href=\"#\"><i class=\"tf-ion-social-dribbble-outline\"></i></a></li>
                            <li><a href=\"#\"><i class=\"tf-ion-social-pinterest-outline\"></i></a></li>-->
                        </ul>
                    </div>
                    <!--/. End Footer Social Links -->

                    <!-- copyright -->
                    <div class=\"copyright text-center\">

                     
                        </p>
                    </div>
                    <!-- /copyright -->

                </div> <!-- end col lg 12 -->
            </div> <!-- end row -->
        </div> <!-- end container -->
    </footer> <!-- end footer -->
    ";
        // line 141
        $this->displayBlock('footerAdd', $context, $blocks);
        // line 142
        echo "</body>
</html>";
    }

    // line 61
    public function block_title($context, array $blocks = [])
    {
    }

    // line 62
    public function block_headAdd($context, array $blocks = [])
    {
    }

    // line 104
    public function block_content($context, array $blocks = [])
    {
    }

    // line 141
    public function block_footerAdd($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 141,  217 => 104,  212 => 62,  207 => 61,  202 => 142,  200 => 141,  162 => 105,  160 => 104,  152 => 98,  149 => 97,  145 => 95,  143 => 94,  138 => 93,  133 => 92,  102 => 63,  100 => 62,  96 => 61,  34 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-138491332-1\"></script>
        <!-- Main jQuery -->
        <script type=\"text/javascript\" src=\"plugins/jquery/dist/jquery.min.js\"></script>
        <script type=\"text/javascript\" src=\"plugins/bootstrap/dist/js/bootstrap.min.js\"></script>
       
       <!-- Slick Carousel -->
    <script type=\"text/javascript\" src=\"plugins/slick-carousel/slick/slick.min.js\"></script>
    <!-- Portfolio Filtering -->
    <script type=\"text/javascript\" src=\"plugins/mixitup/dist/mixitup.min.js\"></script>
    <!-- Smooth Scroll -->
    <script type=\"text/javascript\" src=\"plugins/smooth-scroll/dist/js/smooth-scroll.min.js\"></script>
    <!-- Magnific popup -->
    <script type=\"text/javascript\" src=\"plugins/magnific-popup/dist/jquery.magnific-popup.min.js\"></script>
    <!-- Google Map API -->
    <script type=\"text/javascript\"  src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>
    <!-- Sticky Nav -->
    <script type=\"text/javascript\" src=\"plugins/Sticky/jquery.sticky.js\"></script>
    <!-- Number Counter Script -->
    <script type=\"text/javascript\" src=\"plugins/count-to/jquery.countTo.js\"></script>
    <!-- wow.min Script -->
    <script type=\"text/javascript\" src=\"plugins/wow/dist/wow.min.js\"></script>
    <!-- Custom js -->
    <script type=\"text/javascript\" src=\"js/script.js\"></script>

\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        <meta name=\"description\" content=\"Luxury cars rental in Montreal.\">
        <meta name=\"keywords\" content=\"luxury, car, rent, rent car Montreal, rent cars, rent car, rent cars Montreal, luxury car, luxury cars, BT, BT luxury, luxury cars Montreal, luxury car Montreal, rent luxury car Montreal, rent luxury cars Montreal, BT luxury cars\">
        
        <meta name=\"author\" content=\"Mushu\">
\t\t
        <title>BT Luxury Car Rental Montreal</title>
\t\t
\t\t<!-- Mobile Specific Meta
\t\t================================================== -->
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t
\t\t
\t\t<!-- CSS
\t\t================================================== -->
\t\t<!-- Fontawesome Icon font -->
        <link rel=\"stylesheet\" href=\"plugins/themefisher-font/style.css\">
\t\t<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">
\t\t<!-- bootstrap.min css -->
        <link rel=\"stylesheet\" href=\"plugins/bootstrap/dist/css/bootstrap.min.css\">
\t\t<!-- Animate.css -->
        <link rel=\"stylesheet\" href=\"plugins/animate-css/animate.css\">
        <!-- Magnific popup css -->
        <link rel=\"stylesheet\" href=\"plugins/magnific-popup/dist/magnific-popup.css\">
\t\t<!-- Slick Carousel -->
        <link rel=\"stylesheet\" href=\"plugins/slick-carousel/slick/slick.css\">
        <link rel=\"stylesheet\" href=\"plugins/slick-carousel/slick/slick-theme.css\">
\t\t<!-- Main Stylesheet -->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/myStyle.css\">
        
        <title>{% block title %}{% endblock %}</title>
        {% block headAdd %}{% endblock %}
</head>
<body id=\"body\" data-spy=\"scroll\" data-target=\".navbar\" data-offset=\"50\">

    <header id=\"navigation\" class=\"navbar navigation\">
        <div class=\"container\">
            <div class=\"navbar-header\">

                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>

                <!-- logo -->
                <span class=\"phone align-middle\"> &#9990; +1 514 516 7070</span>
                <span class=\"phone align-middle\"> &#9990; +1 514 573 7567</span>
            </div>

            <!-- main nav -->
            <nav class=\"collapse navbar-collapse navbar-right\" role=\"Navigation\">
                <ul id=\"nav\" class=\"nav navbar-nav navigation-menu\">
                    <li><a data-scroll href=\"/\">Home</a></li>
                    <li><a data-scroll href=\"/#about\">About Us</a></li>
                    <li><a data-scroll href=\"/#portfolio\">Our Cars</a></li>
                    <li><a data-scroll href=\"/#services\">Services</a></li>
                    <li><a data-scroll href=\"/#pricing\">Pricing</a></li>
                    <li><a data-scroll href=\"/#contact-us\">Contact</a></li>
                    <li><a href=\"booking\">Book</a></li>
                    {% if user==null %}<li><a data-scroll href=\"signup\">Sign up</a></li><li><a data-scroll href=\"login\">Log in</a></li>
                    {% else %}<li><a href=\"#\">Welcome back, {{user.userFName}}!</a></li><li><a data-scroll href=\"logout\">Logout</a></li>
                         {% if user.isAdmin %}
                         <li><a data-scroll href=\"management\">Management</a></li>
            {% endif %}
                    {% endif %}
                </ul>
            </nav>
        </div>
    </header>


    <div>{% block content %}{% endblock %}
       
    </div>

  
    <footer id=\"footer\" class=\"bg-one\">
        <div class=\"container\">
            <div class=\"row wow fadeInUp\" data-wow-duration=\"500ms\">
                <div class=\"col-lg-12\">

                    <!-- Footer Social Links -->
                    <div class=\"social-icon\">
                        <ul class=\"list-inline\">
                            <li><a href=\"https://www.facebook.com/BTLuxuryCars/\"><i class=\"tf-ion-social-facebook\"></i></a></li>
                            <!--<li><a href=\"#\"><i class=\"tf-ion-social-twitter\"></i></a></li>
                            <li><a href=\"#\"><i class=\"tf-ion-social-google-outline\"></i></a></li>-->
                            <li><a href=\"https://www.youtube.com/channel/UC25ppQ_5YKSzFWHR6Ge85rg\"><i class=\"tf-ion-social-youtube\"></i></a></li>
                            <li><a href=\"https://www.instagram.com/bt_luxury_cars/\"><i class=\"tf-ion-social-instagram\"></i></a></li>
                            <li><a href=\"#\"><i class=\"tf-ion-social-linkedin\"></i></a></li>
                            <!--<li><a href=\"#\"><i class=\"tf-ion-social-dribbble-outline\"></i></a></li>
                            <li><a href=\"#\"><i class=\"tf-ion-social-pinterest-outline\"></i></a></li>-->
                        </ul>
                    </div>
                    <!--/. End Footer Social Links -->

                    <!-- copyright -->
                    <div class=\"copyright text-center\">

                     
                        </p>
                    </div>
                    <!-- /copyright -->

                </div> <!-- end col lg 12 -->
            </div> <!-- end row -->
        </div> <!-- end container -->
    </footer> <!-- end footer -->
    {% block footerAdd %}{% endblock %}
</body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\project\\templates\\master.html.twig");
    }
}
