<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* step3.html.twig */
class __TwigTemplate_f7033399aaaa55ef79028a81990065570ff35f5137459b64f659b995511d84a4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "step3.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    
    <div class=\"centerContent \">
         
        
        
        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "userFName", []), "html", null, true);
        echo "\" placeholder=\"First name\" type=\"text\" name=\"userFName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      ";
        // line 15
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 16
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "fName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 18
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "userLName", []), "html", null, true);
        echo "\" placeholder=\"Last name\" type=\"text\" name=\"userLName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 24
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 25
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "lName", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 27
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "driverLicense", []), "html", null, true);
        echo "\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 33
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 34
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "driverLicense", []), "html", null, true);
            echo "
                                                         ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "driverLicenseExist", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 37
        echo "                                                      </span>
                                        </div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "address", []), "html", null, true);
        echo "\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 43
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 44
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "address", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 46
        echo "                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "city", []), "html", null, true);
        echo "\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 52
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 53
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "city", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 55
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "province", []), "html", null, true);
        echo "\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 60
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 61
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "province", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 63
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "country", []), "html", null, true);
        echo "\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 68
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 69
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "country", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 71
        echo "                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "postalCode", []), "html", null, true);
        echo "\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 76
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 77
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "postalCode", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 79
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "phoneNumber", []), "html", null, true);
        echo "\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 84
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 85
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "phoneNumber", []), "html", null, true);
            echo "
                                                          ";
        }
        // line 87
        echo "                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      ";
        // line 92
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 93
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "email", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 95
        echo "                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" ";
        // line 100
        echo (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "mailerConsent", [])) ? ("checked") : (""));
        echo "><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div> 
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Next\">
                                        </div>\t\t\t\t\t
\t\t\t\t</form>
\t\t

    </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "step3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 100,  236 => 95,  230 => 93,  228 => 92,  223 => 90,  218 => 87,  212 => 85,  210 => 84,  205 => 82,  200 => 79,  194 => 77,  192 => 76,  187 => 74,  182 => 71,  176 => 69,  174 => 68,  169 => 66,  164 => 63,  158 => 61,  156 => 60,  151 => 58,  146 => 55,  140 => 53,  138 => 52,  133 => 50,  127 => 46,  121 => 44,  119 => 43,  114 => 41,  108 => 37,  103 => 35,  98 => 34,  96 => 33,  91 => 31,  85 => 27,  79 => 25,  77 => 24,  72 => 22,  66 => 18,  60 => 16,  58 => 15,  53 => 13,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
    
    <div class=\"centerContent \">
         
        
        
        <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
\t\t\t\t<form id=\"contact-form\" method=\"post\"  role=\"form\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t  <input class=\"form-control\" value=\"{{v.userFName}}\" placeholder=\"First name\" type=\"text\" name=\"userFName\" size=\"20\">
                                                  <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.fName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.userLName}}\" placeholder=\"Last name\" type=\"text\" name=\"userLName\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.lName}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.driverLicense}}\" placeholder=\"Driver license\" type=\"text\" name=\"driverLicense\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.driverLicense}}
                                                         {{errorList.driverLicenseExist}}
                                                          {% endif %}
                                                      </span>
                                        </div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.address}}\" placeholder=\"Address\" type=\"text\" name=\"address\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.address}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.city}}\" placeholder=\"City\" type=\"text\" name=\"city\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.city}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.province}}\"  placeholder=\"Province\" type=\"text\" name=\"province\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.province}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.country}}\" placeholder=\"Country\" type=\"text\" name=\"country\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.country}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                    <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.postalCode}}\" placeholder=\"Postal code\" type=\"text\" name=\"postalCode\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.postalCode}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t\t<input class=\"form-control\" value=\"{{v.phoneNumber}}\" placeholder=\"Phone number\" type=\"text\" name=\"phoneNumber\" size=\"50\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList.phoneNumber}}
                                                          {% endif %}
                                                      </span>
\t\t\t\t\t</div>
                                     <div class=\"form-group\">
\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email\" value=\"{{v.email}}\" type=\"text\" name=\"email\" size=\"50\">
                                         <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['email']}}
                                                          {% endif %}
                                                      </span>
                                                      <span class=\"errorMessage\" id=\"isTaken\"></span>
\t\t\t\t\t</div>
                                     
                                     <div class=\"form-group\">
                                         <input class=\"\"  type=\"checkbox\" name=\"mailerConsent\" {{v.mailerConsent ? 'checked' : ''}}><span class=\"lables\">Mailer consent<span> 
\t\t\t\t\t</div> 
\t\t\t\t\t
\t\t\t\t\t<div id=\"cf-submit\">
\t\t\t\t\t\t <input id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Next\">
                                        </div>\t\t\t\t\t
\t\t\t\t</form>
\t\t

    </div>
    </div>
{% endblock content %}
", "step3.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\step3.html.twig");
    }
}
