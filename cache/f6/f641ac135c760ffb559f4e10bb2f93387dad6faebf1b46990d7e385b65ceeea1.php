<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* management.html.twig */
class __TwigTemplate_2fbe9cf3eca0086a365797152e8504f43886647fe9169ecc7ad39fb40b7c67b8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "management.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        // line 5
        echo "<div class=\"centerContent\">
    ";
        // line 6
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "isAdmin", [])) {
            // line 7
            echo "      <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
    <form   action=\"/calendar\">
    <input id=\"contact-submit-new\" class=\"btn btn-transparent\"  type=\"submit\" value=\"Calendar\" />
    </form>
          
   
   
    <form  action=\"/chart\">
    <input  id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Chart\" />
    </form>
    
    
    </div>
    ";
        } else {
            // line 21
            echo "<p class=\"centerContent\">You do not have access</p>

";
        }
        // line 24
        echo "    
</div>
";
    }

    public function getTemplateName()
    {
        return "management.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 24,  63 => 21,  47 => 7,  45 => 6,  42 => 5,  39 => 4,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}


{% block content %}
<div class=\"centerContent\">
    {% if user.isAdmin %}
      <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
    <form   action=\"/calendar\">
    <input id=\"contact-submit-new\" class=\"btn btn-transparent\"  type=\"submit\" value=\"Calendar\" />
    </form>
          
   
   
    <form  action=\"/chart\">
    <input  id=\"contact-submit-new\" class=\"btn btn-transparent\" type=\"submit\" value=\"Chart\" />
    </form>
    
    
    </div>
    {% else %}
<p class=\"centerContent\">You do not have access</p>

{% endif %}
    
</div>
{% endblock content %}
", "management.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\management.html.twig");
    }
}
