<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_39d5cfe2db36f8d6bc7e2aca5feddf67e2d0798d90cf73891e92387fa518706a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'footerAdd' => [$this, 'block_footerAdd'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "
    <section class=\"videoStart\">
        <div class=\"overlay\"></div>
        <video playsinline=\"playsinline\" autoplay=\"autoplay\" muted=\"muted\" loop=\"loop\">
            <source src=\"images/backgrounds/MVI_9446.mp4\" type=\"video/mp4\">
            <!--<source src=\"images/backgrounds/backgrounds.mp4\" type=\"video/mp4\">-->
        </video>
        <div class=\"container h-100\">
            <div class=\"d-flex h-100 text-center align-items-center\">
                <div class=\"w-100 text-white\">
                    <h1 class=\"display-3\">BT Luxury Cars Rent Montreal</h1>
                    <p class=\"lead mb-0\">Welcome to the BT family! Based in Montreal, BT Luxury cars will satisfy even the most sophisticated demands and offer you the perfect car for any occasion!
                    </p>
                </div>
            </div>
        </div>
    </section>



    <!-- Start Portfolio Section
                =========================================== -->

    <section class=\"portfolio section\" id=\"portfolio\">
        <div class=\"container\">
            <div class=\"row \" >
                <div class=\"col-lg-12\">

                    <!-- section title -->
                    <div class=\"title text-center\">
                        <h2>Our <span class=\"color\">Cars</span></h2>
                        <div class=\"border\"></div>
                    </div>
                    <!-- /section title -->
                </div> <!-- /end col-lg-12 -->
            </div> <!-- end row -->
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"portfolio-filter\">
                        <button  type=\"button\" data-filter=\"all\">All</button>
                        <button type=\"button\" data-filter=\".Sedan\">Sedan</button>
                        <button type=\"button\" data-filter=\".Coupe\">Coupe</button>
                        <button type=\"button\" data-filter=\".SUV\">SUV</button>
                        <button type=\"button\" data-filter=\".G-CLASS\">G-CLASS</button>
                        <button type=\"button\" data-filter=\".Limousine\">Limousine</button>
                    </div>
                </div>
            </div>
            <div class=\"row portfolio-items-wrapper\">

                <div class=\"mix col-md-4 Sedan\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-1-s.jpg\" alt=\"M52019\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal1\">
                                <span>BMW M5 2019</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 Coupe\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-2-s.jpg\" alt=\"M42017\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal2\">
                                <span>BMW M4 2017</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 Sedan\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-3-s.jpg\" alt=\"B72016\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal3\">
                                <span>BMW Alpina B7 2016</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 SUV\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-4-s.jpg\" alt=\"X52018\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal4\">
                                <span>BMW X5 2018</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 G-CLASS\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-5-s.jpg\" alt=\"Brabus2017\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal5\">
                                <span>G-CLASS 4x4 Brabus 2017</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4  SUV development\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-6-s.jpg\" alt=\"X52016\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal6\">
                                <span>BMW X5 2016</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 SUV\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-7-s.jpg\" alt=\"X5M2016\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal7\">
                                <span>BMW X5M 2016</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 Limousine\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-8-s.jpg\" alt=\"Limo2008\">
                        <div class=\"caption\">
                            <button id=\"limo_btn\" type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal8\">
                                <!--<span>Lincoln Towncar stretch Limo 2008</span>-->
                                <span>Call us now to book!</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>   
        </div>\t<!-- end container -->
    </section>   <!-- End section -->

    <!-- Modal -->
    <div class=\"modal fade\" id=\"Modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle1\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle1\">BMW M5 2019</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button id=\"bookM5\" type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        ";
        // line 163
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 164
            echo "                            ";
            if (($this->getAttribute($context["photo"], "id", []) == 1)) {
                // line 165
                echo "                                <div><img class=\"mySlides\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>

                            ";
            }
            // line 168
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 169
        echo "
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle2\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle2\">BMW M4 2017</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        ";
        // line 191
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 192
            echo "                                ";
            if (($this->getAttribute($context["photo"], "id", []) == 2)) {
                echo "            \t\t\t   
                                    <div><img class=\"mySlides2\" src=\"";
                // line 193
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>

                                ";
            }
            // line 196
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 197
        echo "                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs2(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs2(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal3\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle3\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle3\">BMW Alpina B7 2016</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                         ";
        // line 218
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 219
            echo "                                ";
            if (($this->getAttribute($context["photo"], "id", []) == 3)) {
                echo "            \t\t\t                                       
                                    <div><img class=\"mySlides3\" src=\"";
                // line 220
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>
                                    ";
            }
            // line 222
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "  
                        
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs3(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs3(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal4\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle4\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle4\">BMW X5 2018</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                          ";
        // line 245
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 246
            echo "                                ";
            if (($this->getAttribute($context["photo"], "id", []) == 4)) {
                echo "            \t\t\t                                       

                                    <div><img class=\"mySlides4\" src=\"";
                // line 248
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>
                                    ";
            }
            // line 250
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "  
                       
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs4(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs4(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal5\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle5\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle5\">G-CLASS 4x4 Brabus 2017</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        ";
        // line 273
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 274
            echo "                                ";
            if (($this->getAttribute($context["photo"], "id", []) == 5)) {
                echo "            \t\t\t                                       

                                    <div><img class=\"mySlides5\" src=\"";
                // line 276
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>
                                    ";
            }
            // line 278
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 279
        echo "
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs5(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs5(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal6\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle6\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle6\">BMW X5 2016</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        ";
        // line 301
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 302
            echo "                                ";
            if (($this->getAttribute($context["photo"], "id", []) == 6)) {
                echo "            \t\t\t                                       


                                    <div><img class=\"mySlides6\" src=\"";
                // line 305
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>
                                    ";
            }
            // line 307
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 308
        echo "                        <!--<button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs6(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs6(1)\">&#10095;</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal7\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle7\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle7\">BMW X5 2016</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                         ";
        // line 329
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 330
            echo "                                    ";
            if (($this->getAttribute($context["photo"], "id", []) == 7)) {
                echo "            \t\t\t                                       

                                        <div><img class=\"mySlides7\" src=\"";
                // line 332
                echo twig_escape_filter($this->env, $this->getAttribute($context["photo"], "pathPhoto", []), "html", null, true);
                echo "\"></div>

                                    ";
            }
            // line 335
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 336
        echo "                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs7(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs7(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- About Us Section-->
    <section class=\"section about-2 padding-0 bg-dark\" id=\"about\">
        <div class=\"container-fluid \">
            <div class=\"row \" >
                <div class=\"col-lg-12\">
                    <div class=\"title text-center\">
                        <h2>About <span class=\"color\">Us</span></h2>
                        <div class=\"border\"></div>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 padding-0\">
                    <img class=\"img-responsive\" src=\"images/about/IMG_9420.1-min.jpg\" alt=\"Company\">
                </div>
                <div class=\"col-md-6\">
                    <div class=\"content-block\">
                        <h2>BT Luxury company mission and values</h2>
                        <p>Nothing can emphasize the status, style and taste of a person as well as a car does, especially a luxury car.
                            A luxury car will always catch other`s eyes and be followed.
                            A luxury car guarantees maximum comfort during a trip and emphasizes the individuality of the person who drives it.
                            Therefore, the principle of our company is based primarily on a personal approach to our customers whom we strive to establish stable and long-term relationships with.
                            This allows <strong>BT Luxury Cars</strong> to be one of the most dynamically developing companies in the field of luxury cars rental business.
                        </p>
                        <p>
                            We believe that all our clients deserve only the royal level service and guarantee that their demands will not only be satisfied, but their expectations will be exceeded. 
                        </p>
                        <p>
                            <strong>BT Luxury Cars'</strong> mission is to serve our customers at the highest level and provide them with the opportunity to rent the best luxury cars.
                            Driving any of our cars on the picturesque roads of Canada is a special and pleasurable experience that cannot be compared to anything!
                            In addition to this, you can always rent a car with a professional driver who will ensure a comfortable and safe ride.
                            We are constantly challenging ourselves with searching for and finding opportunities to enter new markets and increase the number of offers for our clients.
                        </p>
                        <a class=\"collapsible\"><strong>Read more</strong></a>
                        <div class=\"content\"><br/>
                            <p>
                                Let us meet you and your honorable guests with dignity and pride.
                                Our cars will help you feel confident and comfortable at any important meeting or business negotiations.
                                <strong>Think, live and be VIP. Life is good, enjoy it!</strong>
                                <img class=\"img BTlogo\" src=\"images/logo.png\" alt=\"Company\" align=\"right\">
                            </p>
                            <h4>
                                Why People choose BT Luxury?
                            </h4>
                            <ul id='whyChoose'>
                                <li>Our clients get the best service.</li>
                                <li>We provide cars only in excellent condition.</li>
                                <li>We provide 24/7 support for each client.</li>
                                <li>Our drivers can bring a car at any time.</li>
                                <li>Businessmen, celebrities and politicians use our services.</li>
                                <li>We offer chauffeur services for various hotels, large international embassies, etc.</li>
                                <li>Our drivers are real professionals.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id=\"services\" class=\"bg-one section\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- section title -->
                <div class=\"title text-center wow fadeIn\" data-wow-duration=\"500ms\">
                    <h2>Our <span class=\"color\">Services</span></h2>
                    <div class=\"border\"></div>
                </div>
                <!-- /section title -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class='fas fa-american-sign-language-interpreting'></i>
                        </div>
                        <h3>WEDDINGS</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"200ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-ion-laptop\"></i>
                        </div>
                        <h3>GRADUATION</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-genius\"></i>
                        </div>
                        <h3>BIRTHDAY PARTIES</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"200ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-dial\"></i>
                        </div>
                        <h3>BAR / BAT MITZVAH</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-target3\"></i>
                        </div>
                        <h3>EXCLUSIVE PARTIES</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"600ms\">
                    <div class=\"service-block text-center kill-margin-bottom\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-lifesaver\"></i>
                        </div>
                        <h3>BUSINESS TRIPS</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->\t

            </div> \t\t<!-- End row -->
        </div>   \t<!-- End container -->
    </section>   <!-- End section -->


    <!-- Start Pricing section
                    =========================================== -->

    <section id=\"pricing\" class=\"pricing section\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- section title -->
                <div class=\"title text-center wow fadeInDown\" data-wow-duration=\"500ms\">
                    <h2>Pricing<span class=\"color\"> Plans</span></h2>
                    <div class=\"border\"></div>
                </div>
                <!-- /section title -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"200ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>Coupe</h3>
                            <p><strong class=\"value\">From 400\$ </strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>

                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>Sedan</h3>
                            <p><strong class=\"value\">From \$450</strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"600ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>SUV</h3>
                            <p><strong class=\"value\">From \$350</strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"750ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>G-CLASS</h3>
                            <p><strong class=\"value\">From \$2000</strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"750ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>Limousine</h3>
                            <p><strong class=\"value\">From \$120</strong>/ Hour</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>Minimum 4 hour </li>
                            <li>Bottle of champagne</li>
                            <li>Ice and glasses</li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"#contact-us\">Call us now to book</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->


            </div>       <!-- End row -->
        </div>   \t<!-- End container -->
    </section>   <!-- End section -->

    <!--
Start Counter Section
==================================== -->

    <section id=\"counter\" class=\"parallax-section bg-1 section overly\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- first count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\">
                    <div class=\"counters-item\">
                        <i class=\"tf-ion-android-happy\"></i>
                        <span data-speed=\"3000\" data-to=\"634\">243</span>
                        <h3>Happy Clients</h3>
                    </div>
                </div>
                <!-- end first count item -->

                <!-- second count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\" data-wow-delay=\"200ms\">
                    <div class=\"counters-item\">
                        <i class=\"tf-ion-archive\"></i>
                        <span data-speed=\"3000\" data-to=\"217\">82</span>
                        <h3>Birthday Parties</h3>
                    </div>
                </div>
                <!-- end second count item -->

                <!-- third count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"counters-item\">
                        <i class=\"tf-ion-thumbsup\"></i>
                        <span data-speed=\"3000\" data-to=\"27\">29</span>
                        <h3>Weddings</h3>

                    </div>
                </div>
                <!-- end third count item -->

                <!-- fourth count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\" data-wow-delay=\"600ms\">
                    <div class=\"counters-item kill-margin-bottom\">
                        <i class=\"tf-ion-coffee\"></i>
                        <span data-speed=\"3000\" data-to=\"2500\">21</span>
                        <h3>Graduation</h3>
                    </div>
                </div>
                <!-- end fourth count item -->

            </div> \t\t<!-- end row -->
        </div>   \t<!-- end container -->
        <!--</section>    end section -->

        <!-- Start Testimonial
                        =========================================== -->

        <div class=\"container\">
            <div class=\"row\">\t\t\t\t
                <div class=\"col-lg-12\">
                    <!-- testimonial wrapper -->
                    <div id=\"testimonials\" class=\"wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"100ms\">

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/1.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Christine</h3>
                                    <span>Jan 9, 2018</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>I had a wonderful experience! Everyone was super helpful and couldn’t have hoped for a better car and price. 
                                        My overall experience was perfect. Didn’t have any problems! I will definitely be coming back to try other cars.</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/5.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Artur</h3>
                                    <span>Feb 23, 2019</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>I tried the Brabus 4x4 and it was amazing! I had lots of fun driving this monster truck and got it for the best price possible. I would definitely recommend BT Luxury cars to everyone who wants to get a luxury experience!</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/2.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Brunet Vincent William</h3>
                                    <span>Feb 5, 2019</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>Nice cars - good service - love it</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/4.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>John</h3>
                                    <span>Oct 21, 2018</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>My friend suggested to get a car from BT Luxury Cars and I am extremely glad I listened to him. The BMW M5 I drove was exceptional. I will definitely rent again from you guys!</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/3.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Jonathon Andrew</h3>
                                    <span>Oct 11, 2018</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>Awesome for an unforgetable experience: graduation, marriage proposal, and so on</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                    </div>\t\t<!-- end testimonial wrapper -->
                </div> \t\t<!-- end col lg 12 -->
            </div>\t    <!-- End row -->
        </div>       <!-- End container -->
    </section>    <!-- End Section -->

    <!-- Srart Contact Us
    =========================================== -->\t\t
    <section id=\"contact-us\" class=\"contact-us section-bg\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- section title -->
                <div class=\"title text-center wow fadeIn\" data-wow-duration=\"500ms\">
                    <h2>Get In <span class=\"color\">Touch</span></h2>
                    <div class=\"border\"></div>
                </div>
                <!-- /section title -->

                <!-- Contact Details -->
                <div class=\"contact-info col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\">
                    <h3>Contact Details</h3>
                    <p>Based in Montreal, BT Luxury cars will satisfy even the most sophisticated demands and offer you the perfect car for any occasion!</p>
                    <p>
                        Be that something exotic for a special weekend, something exclusive for an important business meeting or just something sporty for fun – whatever your reason is, BT Luxury`s  very special fleet selection is always there to assist!

                    </p>

                    <p>Contact us now to learn more.</p>
                    <div class=\"contact-details\">
                        <div class=\"con-info clearfix\">
                            <i class=\"tf-map-pin\"></i>
                            <span>Rue du Fort Rolland, Montreal, Quebec, Canada</span>
                        </div>

                        <div class=\"con-info clearfix\">
                            <i class=\"tf-ion-ios-telephone-outline\"></i>
                            <span>Phone: +1 514 516 7070</span>
                        </div>

                        <div class=\"con-info clearfix\">
                            <i class=\"tf-ion-iphone\"></i>
                            <span>Phone: +1 514 573 7567</span>
                        </div>

                        <div class=\"con-info clearfix\">
                            <i class=\"tf-ion-ios-email-outline\"></i>
                            <span>Email: btluxurycars@gmail.com</span>
                        </div>
                    </div>
                </div>
                <!-- / End Contact Details -->

                <!-- Contact Form -->
            <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
                <form id=\"contact-form\" method=\"post\" role=\"form\">
                                
                    <div class=\"form-group\">
                        <input type=\"text\" placeholder=\"Your Name\" class=\"form-control\" name=\"name\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 886
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 887
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "name", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 889
        echo "                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <input type=\"email\" placeholder=\"Your Email\" class=\"form-control\" name=\"email\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 895
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 896
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "email", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 898
        echo "                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <input type=\"tel\" placeholder=\"Your Phone\" class=\"form-control\" name=\"phone\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 904
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 905
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "phone", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 907
        echo "                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <input type=\"text\" placeholder=\"Subject\" class=\"form-control\" name=\"subject\">
                                                <span class=\"erromessage\">
                                                      ";
        // line 913
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 914
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "subject", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 916
        echo "                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <textarea rows=\"6\" placeholder=\"Message\" class=\"form-control\" name=\"message\"></textarea>
                                                <span class=\"erromessage\">
                                                      ";
        // line 922
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 923
            echo "                                                         ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errorList"]) ? $context["errorList"] : null), "message", [], "array"), "html", null, true);
            echo "
                                                          ";
        }
        // line 925
        echo "                                                      </span>
                    </div>
                    
                                                      <input name=\"checkHuman\" type=\"text\" style=\"display:none\" value=\"\" /><br>
                    
                                                      <p>";
        // line 930
        echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : null), "html", null, true);
        echo "</p>
                    
                    <div id=\"cf-submit\">
                        <input type=\"submit\" id=\"contact-submit-new\" class=\"btn btn-transparent\" value=\"Submit\">
                                            
                </form>
        </div>
                
            </div>
            
    <!-- ./End Contact Form -->

            </div> <!-- end row -->
        </div> <!-- end container -->

    </section> <!-- end section -->





    <!-- end Contact Area
    ========================================== -->

";
    }

    // line 956
    public function block_footerAdd($context, array $blocks = [])
    {
        // line 957
        echo "    Essential Scripts
    =====================================-->

    <script>
        var slideIndex = 1;
        showDivs(slideIndex);
        showDivs2(slideIndex);
        showDivs3(slideIndex);
        showDivs4(slideIndex);
        showDivs5(slideIndex);
        showDivs6(slideIndex);
        showDivs7(slideIndex);

        function plusDivs(n) {
            showDivs(slideIndex += n);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs2(n) {
            showDivs2(slideIndex += n);
        }

        function showDivs2(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides2\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs3(n) {
            showDivs3(slideIndex += n);
        }

        function showDivs3(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides3\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs4(n) {
            showDivs4(slideIndex += n);
        }

        function showDivs4(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides4\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs5(n) {
            showDivs5(slideIndex += n);
        }

        function showDivs5(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides5\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs6(n) {
            showDivs6(slideIndex += n);
        }

        function showDivs6(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides6\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs7(n) {
            showDivs7(slideIndex += n);
        }

        function showDivs7(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides7\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        \$(document).click(function (e) {
            var container = \$('.modal-content');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal2');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal2('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal3');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal3('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal4');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal4('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal5');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal5('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal6');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal6('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal7');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal7('close');
            }
        });

        var coll = document.getElementsByClassName(\"collapsible\");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener(\"click\", function () {
                this.classList.toggle(\"active\");
                var content = this.nextElementSibling;
                if (content.style.display === \"block\") {
                    content.style.display = \"none\";
                } else {
                    content.style.display = \"block\";
                }
            });
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1154 => 957,  1151 => 956,  1122 => 930,  1115 => 925,  1109 => 923,  1107 => 922,  1099 => 916,  1093 => 914,  1091 => 913,  1083 => 907,  1077 => 905,  1075 => 904,  1067 => 898,  1061 => 896,  1059 => 895,  1051 => 889,  1045 => 887,  1043 => 886,  491 => 336,  485 => 335,  479 => 332,  473 => 330,  469 => 329,  446 => 308,  440 => 307,  435 => 305,  428 => 302,  424 => 301,  400 => 279,  394 => 278,  389 => 276,  383 => 274,  379 => 273,  349 => 250,  344 => 248,  338 => 246,  334 => 245,  304 => 222,  299 => 220,  294 => 219,  290 => 218,  267 => 197,  261 => 196,  255 => 193,  250 => 192,  246 => 191,  222 => 169,  216 => 168,  209 => 165,  206 => 164,  202 => 163,  43 => 6,  40 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}



{% block content %}

    <section class=\"videoStart\">
        <div class=\"overlay\"></div>
        <video playsinline=\"playsinline\" autoplay=\"autoplay\" muted=\"muted\" loop=\"loop\">
            <source src=\"images/backgrounds/MVI_9446.mp4\" type=\"video/mp4\">
            <!--<source src=\"images/backgrounds/backgrounds.mp4\" type=\"video/mp4\">-->
        </video>
        <div class=\"container h-100\">
            <div class=\"d-flex h-100 text-center align-items-center\">
                <div class=\"w-100 text-white\">
                    <h1 class=\"display-3\">BT Luxury Cars Rent Montreal</h1>
                    <p class=\"lead mb-0\">Welcome to the BT family! Based in Montreal, BT Luxury cars will satisfy even the most sophisticated demands and offer you the perfect car for any occasion!
                    </p>
                </div>
            </div>
        </div>
    </section>



    <!-- Start Portfolio Section
                =========================================== -->

    <section class=\"portfolio section\" id=\"portfolio\">
        <div class=\"container\">
            <div class=\"row \" >
                <div class=\"col-lg-12\">

                    <!-- section title -->
                    <div class=\"title text-center\">
                        <h2>Our <span class=\"color\">Cars</span></h2>
                        <div class=\"border\"></div>
                    </div>
                    <!-- /section title -->
                </div> <!-- /end col-lg-12 -->
            </div> <!-- end row -->
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"portfolio-filter\">
                        <button  type=\"button\" data-filter=\"all\">All</button>
                        <button type=\"button\" data-filter=\".Sedan\">Sedan</button>
                        <button type=\"button\" data-filter=\".Coupe\">Coupe</button>
                        <button type=\"button\" data-filter=\".SUV\">SUV</button>
                        <button type=\"button\" data-filter=\".G-CLASS\">G-CLASS</button>
                        <button type=\"button\" data-filter=\".Limousine\">Limousine</button>
                    </div>
                </div>
            </div>
            <div class=\"row portfolio-items-wrapper\">

                <div class=\"mix col-md-4 Sedan\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-1-s.jpg\" alt=\"M52019\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal1\">
                                <span>BMW M5 2019</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 Coupe\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-2-s.jpg\" alt=\"M42017\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal2\">
                                <span>BMW M4 2017</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 Sedan\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-3-s.jpg\" alt=\"B72016\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal3\">
                                <span>BMW Alpina B7 2016</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 SUV\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-4-s.jpg\" alt=\"X52018\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal4\">
                                <span>BMW X5 2018</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 G-CLASS\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-5-s.jpg\" alt=\"Brabus2017\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal5\">
                                <span>G-CLASS 4x4 Brabus 2017</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4  SUV development\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-6-s.jpg\" alt=\"X52016\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal6\">
                                <span>BMW X5 2016</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 SUV\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-7-s.jpg\" alt=\"X5M2016\">
                        <div class=\"caption\">
                            <button type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal7\">
                                <span>BMW X5M 2016</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class=\"mix col-md-4 Limousine\">
                    <div class=\"portfolio-block\">
                        <img class=\"img-responsive\" src=\"images/portfolio/portfolio-8-s.jpg\" alt=\"Limo2008\">
                        <div class=\"caption\">
                            <button id=\"limo_btn\" type=\"button\" class=\"btn btn-primary\" data-filter=\"all\" data-toggle=\"modal\" data-target=\"#Modal8\">
                                <!--<span>Lincoln Towncar stretch Limo 2008</span>-->
                                <span>Call us now to book!</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>   
        </div>\t<!-- end container -->
    </section>   <!-- End section -->

    <!-- Modal -->
    <div class=\"modal fade\" id=\"Modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle1\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle1\">BMW M5 2019</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button id=\"bookM5\" type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        {% for photo in photos %}
                            {% if photo.id == 1 %}
                                <div><img class=\"mySlides\" src=\"{{photo.pathPhoto}}\"></div>

                            {% endif %}
                        {% endfor %}

                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle2\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle2\">BMW M4 2017</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        {% for photo in photos %}
                                {% if photo.id == 2 %}            \t\t\t   
                                    <div><img class=\"mySlides2\" src=\"{{photo.pathPhoto}}\"></div>

                                {% endif %}
                            {% endfor %}
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs2(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs2(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal3\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle3\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle3\">BMW Alpina B7 2016</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                         {% for photo in photos %}
                                {% if photo.id == 3 %}            \t\t\t                                       
                                    <div><img class=\"mySlides3\" src=\"{{photo.pathPhoto}}\"></div>
                                    {% endif %}
                                {% endfor %}  
                        
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs3(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs3(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal4\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle4\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle4\">BMW X5 2018</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                          {% for photo in photos %}
                                {% if photo.id == 4 %}            \t\t\t                                       

                                    <div><img class=\"mySlides4\" src=\"{{photo.pathPhoto}}\"></div>
                                    {% endif %}
                                {% endfor %}  
                       
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs4(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs4(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal5\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle5\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle5\">G-CLASS 4x4 Brabus 2017</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        {% for photo in photos %}
                                {% if photo.id == 5 %}            \t\t\t                                       

                                    <div><img class=\"mySlides5\" src=\"{{photo.pathPhoto}}\"></div>
                                    {% endif %}
                                {% endfor %}

                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs5(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs5(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal6\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle6\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle6\">BMW X5 2016</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                        {% for photo in photos %}
                                {% if photo.id == 6 %}            \t\t\t                                       


                                    <div><img class=\"mySlides6\" src=\"{{photo.pathPhoto}}\"></div>
                                    {% endif %}
                                {% endfor %}
                        <!--<button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs6(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs6(1)\">&#10095;</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"Modal7\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle7\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"exampleModalLongTitle7\">BMW X5 2016</h5>
                    <button type=\"button\" class=\"btn btn-secondary closeCarous\" data-dismiss=\"modal\">Close
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <button type=\"button\" onclick=\"window.location.href = 'booking';\" class=\"btn btn-secondary closeCarous Book\">Book
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div>
                         {% for photo in photos %}
                                    {% if photo.id == 7 %}            \t\t\t                                       

                                        <div><img class=\"mySlides7\" src=\"{{photo.pathPhoto}}\"></div>

                                    {% endif %}
                                {% endfor %}
                        <button class=\"w3-button w3-black w3-display-left \" onclick=\"plusDivs7(-1)\">&#10094;</button>
                        <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs7(1)\">&#10095;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- About Us Section-->
    <section class=\"section about-2 padding-0 bg-dark\" id=\"about\">
        <div class=\"container-fluid \">
            <div class=\"row \" >
                <div class=\"col-lg-12\">
                    <div class=\"title text-center\">
                        <h2>About <span class=\"color\">Us</span></h2>
                        <div class=\"border\"></div>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 padding-0\">
                    <img class=\"img-responsive\" src=\"images/about/IMG_9420.1-min.jpg\" alt=\"Company\">
                </div>
                <div class=\"col-md-6\">
                    <div class=\"content-block\">
                        <h2>BT Luxury company mission and values</h2>
                        <p>Nothing can emphasize the status, style and taste of a person as well as a car does, especially a luxury car.
                            A luxury car will always catch other`s eyes and be followed.
                            A luxury car guarantees maximum comfort during a trip and emphasizes the individuality of the person who drives it.
                            Therefore, the principle of our company is based primarily on a personal approach to our customers whom we strive to establish stable and long-term relationships with.
                            This allows <strong>BT Luxury Cars</strong> to be one of the most dynamically developing companies in the field of luxury cars rental business.
                        </p>
                        <p>
                            We believe that all our clients deserve only the royal level service and guarantee that their demands will not only be satisfied, but their expectations will be exceeded. 
                        </p>
                        <p>
                            <strong>BT Luxury Cars'</strong> mission is to serve our customers at the highest level and provide them with the opportunity to rent the best luxury cars.
                            Driving any of our cars on the picturesque roads of Canada is a special and pleasurable experience that cannot be compared to anything!
                            In addition to this, you can always rent a car with a professional driver who will ensure a comfortable and safe ride.
                            We are constantly challenging ourselves with searching for and finding opportunities to enter new markets and increase the number of offers for our clients.
                        </p>
                        <a class=\"collapsible\"><strong>Read more</strong></a>
                        <div class=\"content\"><br/>
                            <p>
                                Let us meet you and your honorable guests with dignity and pride.
                                Our cars will help you feel confident and comfortable at any important meeting or business negotiations.
                                <strong>Think, live and be VIP. Life is good, enjoy it!</strong>
                                <img class=\"img BTlogo\" src=\"images/logo.png\" alt=\"Company\" align=\"right\">
                            </p>
                            <h4>
                                Why People choose BT Luxury?
                            </h4>
                            <ul id='whyChoose'>
                                <li>Our clients get the best service.</li>
                                <li>We provide cars only in excellent condition.</li>
                                <li>We provide 24/7 support for each client.</li>
                                <li>Our drivers can bring a car at any time.</li>
                                <li>Businessmen, celebrities and politicians use our services.</li>
                                <li>We offer chauffeur services for various hotels, large international embassies, etc.</li>
                                <li>Our drivers are real professionals.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id=\"services\" class=\"bg-one section\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- section title -->
                <div class=\"title text-center wow fadeIn\" data-wow-duration=\"500ms\">
                    <h2>Our <span class=\"color\">Services</span></h2>
                    <div class=\"border\"></div>
                </div>
                <!-- /section title -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class='fas fa-american-sign-language-interpreting'></i>
                        </div>
                        <h3>WEDDINGS</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"200ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-ion-laptop\"></i>
                        </div>
                        <h3>GRADUATION</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-genius\"></i>
                        </div>
                        <h3>BIRTHDAY PARTIES</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"200ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-dial\"></i>
                        </div>
                        <h3>BAR / BAT MITZVAH</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"service-block text-center\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-target3\"></i>
                        </div>
                        <h3>EXCLUSIVE PARTIES</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <article class=\"col-md-4 col-sm-6 col-xs-12 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"600ms\">
                    <div class=\"service-block text-center kill-margin-bottom\">
                        <div class=\"service-icon text-center\">
                            <i class=\"tf-lifesaver\"></i>
                        </div>
                        <h3>BUSINESS TRIPS</h3>
                        <p>*************************************************</p>
                    </div>
                </article>
                <!-- End Single Service Item -->\t

            </div> \t\t<!-- End row -->
        </div>   \t<!-- End container -->
    </section>   <!-- End section -->


    <!-- Start Pricing section
                    =========================================== -->

    <section id=\"pricing\" class=\"pricing section\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- section title -->
                <div class=\"title text-center wow fadeInDown\" data-wow-duration=\"500ms\">
                    <h2>Pricing<span class=\"color\"> Plans</span></h2>
                    <div class=\"border\"></div>
                </div>
                <!-- /section title -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"200ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>Coupe</h3>
                            <p><strong class=\"value\">From 400\$ </strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>

                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>Sedan</h3>
                            <p><strong class=\"value\">From \$450</strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"600ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>SUV</h3>
                            <p><strong class=\"value\">From \$350</strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"750ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>G-CLASS</h3>
                            <p><strong class=\"value\">From \$2000</strong>/ Day</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>24 hour </li>
                            <li>Included 200km / day</li>
                            <li>Extra mileage is 2\$ / km </li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"booking\">BOOK NOW</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->

                <!-- single pricing table -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"750ms\">
                    <div class=\"price-item\">

                        <!-- plan name & value -->
                        <div class=\"price-title\">
                            <h3>Limousine</h3>
                            <p><strong class=\"value\">From \$120</strong>/ Hour</p>
                        </div>
                        <!-- /plan name & value -->

                        <!-- plan description -->
                        <ul>
                            <li>Minimum 4 hour </li>
                            <li>Bottle of champagne</li>
                            <li>Ice and glasses</li>
                        </ul>
                        <!-- /plan description -->

                        <!-- signup button -->
                        <a class=\"btn btn-transparent\" href=\"#contact-us\">Call us now to book</a>
                        <!-- /signup button -->

                    </div>
                </div>
                <!-- end single pricing table -->


            </div>       <!-- End row -->
        </div>   \t<!-- End container -->
    </section>   <!-- End section -->

    <!--
Start Counter Section
==================================== -->

    <section id=\"counter\" class=\"parallax-section bg-1 section overly\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- first count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\">
                    <div class=\"counters-item\">
                        <i class=\"tf-ion-android-happy\"></i>
                        <span data-speed=\"3000\" data-to=\"634\">243</span>
                        <h3>Happy Clients</h3>
                    </div>
                </div>
                <!-- end first count item -->

                <!-- second count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\" data-wow-delay=\"200ms\">
                    <div class=\"counters-item\">
                        <i class=\"tf-ion-archive\"></i>
                        <span data-speed=\"3000\" data-to=\"217\">82</span>
                        <h3>Birthday Parties</h3>
                    </div>
                </div>
                <!-- end second count item -->

                <!-- third count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\" data-wow-delay=\"400ms\">
                    <div class=\"counters-item\">
                        <i class=\"tf-ion-thumbsup\"></i>
                        <span data-speed=\"3000\" data-to=\"27\">29</span>
                        <h3>Weddings</h3>

                    </div>
                </div>
                <!-- end third count item -->

                <!-- fourth count item -->
                <div class=\"col-md-3 col-sm-6 col-xs-12 text-center wow fadeInDown\" data-wow-duration=\"500ms\" data-wow-delay=\"600ms\">
                    <div class=\"counters-item kill-margin-bottom\">
                        <i class=\"tf-ion-coffee\"></i>
                        <span data-speed=\"3000\" data-to=\"2500\">21</span>
                        <h3>Graduation</h3>
                    </div>
                </div>
                <!-- end fourth count item -->

            </div> \t\t<!-- end row -->
        </div>   \t<!-- end container -->
        <!--</section>    end section -->

        <!-- Start Testimonial
                        =========================================== -->

        <div class=\"container\">
            <div class=\"row\">\t\t\t\t
                <div class=\"col-lg-12\">
                    <!-- testimonial wrapper -->
                    <div id=\"testimonials\" class=\"wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"100ms\">

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/1.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Christine</h3>
                                    <span>Jan 9, 2018</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>I had a wonderful experience! Everyone was super helpful and couldn’t have hoped for a better car and price. 
                                        My overall experience was perfect. Didn’t have any problems! I will definitely be coming back to try other cars.</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/5.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Artur</h3>
                                    <span>Feb 23, 2019</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>I tried the Brabus 4x4 and it was amazing! I had lots of fun driving this monster truck and got it for the best price possible. I would definitely recommend BT Luxury cars to everyone who wants to get a luxury experience!</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/2.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Brunet Vincent William</h3>
                                    <span>Feb 5, 2019</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>Nice cars - good service - love it</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/4.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>John</h3>
                                    <span>Oct 21, 2018</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>My friend suggested to get a car from BT Luxury Cars and I am extremely glad I listened to him. The BMW M5 I drove was exceptional. I will definitely rent again from you guys!</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                        <!-- testimonial single -->
                        <div class=\"item text-center\">

                            <!-- client photo -->
                            <div class=\"client-thumb\">
                                <img src=\"images/team/3.jpg\" class=\"img-responsive\" alt=\"Meghna\">
                            </div>
                            <!-- /client photo -->

                            <!-- client info -->
                            <div class=\"client-info\">
                                <div class=\"client-meta\">
                                    <h3>Jonathon Andrew</h3>
                                    <span>Oct 11, 2018</span>
                                </div>
                                <div class=\"client-comment\">
                                    <p>Awesome for an unforgetable experience: graduation, marriage proposal, and so on</p>
                                </div>
                            </div>
                            <!-- /client info -->
                        </div>
                        <!-- /testimonial single -->

                    </div>\t\t<!-- end testimonial wrapper -->
                </div> \t\t<!-- end col lg 12 -->
            </div>\t    <!-- End row -->
        </div>       <!-- End container -->
    </section>    <!-- End Section -->

    <!-- Srart Contact Us
    =========================================== -->\t\t
    <section id=\"contact-us\" class=\"contact-us section-bg\">
        <div class=\"container\">
            <div class=\"row\">

                <!-- section title -->
                <div class=\"title text-center wow fadeIn\" data-wow-duration=\"500ms\">
                    <h2>Get In <span class=\"color\">Touch</span></h2>
                    <div class=\"border\"></div>
                </div>
                <!-- /section title -->

                <!-- Contact Details -->
                <div class=\"contact-info col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\">
                    <h3>Contact Details</h3>
                    <p>Based in Montreal, BT Luxury cars will satisfy even the most sophisticated demands and offer you the perfect car for any occasion!</p>
                    <p>
                        Be that something exotic for a special weekend, something exclusive for an important business meeting or just something sporty for fun – whatever your reason is, BT Luxury`s  very special fleet selection is always there to assist!

                    </p>

                    <p>Contact us now to learn more.</p>
                    <div class=\"contact-details\">
                        <div class=\"con-info clearfix\">
                            <i class=\"tf-map-pin\"></i>
                            <span>Rue du Fort Rolland, Montreal, Quebec, Canada</span>
                        </div>

                        <div class=\"con-info clearfix\">
                            <i class=\"tf-ion-ios-telephone-outline\"></i>
                            <span>Phone: +1 514 516 7070</span>
                        </div>

                        <div class=\"con-info clearfix\">
                            <i class=\"tf-ion-iphone\"></i>
                            <span>Phone: +1 514 573 7567</span>
                        </div>

                        <div class=\"con-info clearfix\">
                            <i class=\"tf-ion-ios-email-outline\"></i>
                            <span>Email: btluxurycars@gmail.com</span>
                        </div>
                    </div>
                </div>
                <!-- / End Contact Details -->

                <!-- Contact Form -->
            <div class=\"contact-form col-md-6 wow fadeInUp\" data-wow-duration=\"500ms\" data-wow-delay=\"300ms\">
                <form id=\"contact-form\" method=\"post\" role=\"form\">
                                
                    <div class=\"form-group\">
                        <input type=\"text\" placeholder=\"Your Name\" class=\"form-control\" name=\"name\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['name']}}
                                                          {% endif %}
                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <input type=\"email\" placeholder=\"Your Email\" class=\"form-control\" name=\"email\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['email']}}
                                                          {% endif %}
                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <input type=\"tel\" placeholder=\"Your Phone\" class=\"form-control\" name=\"phone\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['phone']}}
                                                          {% endif %}
                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <input type=\"text\" placeholder=\"Subject\" class=\"form-control\" name=\"subject\">
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['subject']}}
                                                          {% endif %}
                                                      </span>
                    </div>
                    
                    <div class=\"form-group\">
                        <textarea rows=\"6\" placeholder=\"Message\" class=\"form-control\" name=\"message\"></textarea>
                                                <span class=\"erromessage\">
                                                      {% if errorList %}
                                                         {{errorList['message']}}
                                                          {% endif %}
                                                      </span>
                    </div>
                    
                                                      <input name=\"checkHuman\" type=\"text\" style=\"display:none\" value=\"\" /><br>
                    
                                                      <p>{{result}}</p>
                    
                    <div id=\"cf-submit\">
                        <input type=\"submit\" id=\"contact-submit-new\" class=\"btn btn-transparent\" value=\"Submit\">
                                            
                </form>
        </div>
                
            </div>
            
    <!-- ./End Contact Form -->

            </div> <!-- end row -->
        </div> <!-- end container -->

    </section> <!-- end section -->





    <!-- end Contact Area
    ========================================== -->

{% endblock content %}

{% block footerAdd %}
    Essential Scripts
    =====================================-->

    <script>
        var slideIndex = 1;
        showDivs(slideIndex);
        showDivs2(slideIndex);
        showDivs3(slideIndex);
        showDivs4(slideIndex);
        showDivs5(slideIndex);
        showDivs6(slideIndex);
        showDivs7(slideIndex);

        function plusDivs(n) {
            showDivs(slideIndex += n);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs2(n) {
            showDivs2(slideIndex += n);
        }

        function showDivs2(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides2\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs3(n) {
            showDivs3(slideIndex += n);
        }

        function showDivs3(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides3\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs4(n) {
            showDivs4(slideIndex += n);
        }

        function showDivs4(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides4\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs5(n) {
            showDivs5(slideIndex += n);
        }

        function showDivs5(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides5\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs6(n) {
            showDivs6(slideIndex += n);
        }

        function showDivs6(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides6\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        function plusDivs7(n) {
            showDivs7(slideIndex += n);
        }

        function showDivs7(n) {
            var i;
            var x = document.getElementsByClassName(\"mySlides7\");
            if (n > x.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = x.length;
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = \"none\";
            }
            x[slideIndex - 1].style.display = \"block\";
        }

        \$(document).click(function (e) {
            var container = \$('.modal-content');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal2');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal2('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal3');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal3('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal4');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal4('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal5');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal5('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal6');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal6('close');
            }
        });

        \$(document).click(function (e) {
            var container = \$('#Modal7');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.Modal7('close');
            }
        });

        var coll = document.getElementsByClassName(\"collapsible\");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener(\"click\", function () {
                this.classList.toggle(\"active\");
                var content = this.nextElementSibling;
                if (content.style.display === \"block\") {
                    content.style.display = \"none\";
                } else {
                    content.style.display = \"block\";
                }
            });
        }
    </script>
{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\ipd17-project\\templates\\index.html.twig");
    }
}
