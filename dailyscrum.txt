<< current date goes here >>
1. What I have done / not done since last Scrum?
2. What I plan to do from this scrum to next? (next 24 hours)
3. Where do I need assistance? What do I need to figure out?

June 21, 2019

Shagha
1. 
2. a) Create database and tables
    b) Trello admin part
    c)  Login / Logout
    d) Sign up
    e)  Create twig master template
    f)  Exchange main page codes
3. Read about PHP form wizards
   

---
Marina
1.
2. a) Fill up database with data
    b) Trello user part
    c) Insert the main page into twig template
    d) Add login, signup, book buttons to the main page
    e) Exchange main page codes
3. Read about PHP form wizards

June 25, 2019

Shagha
1.
    a) I created database and tables
    b) Trello admin part
    c)  Login / Logout
    d) Sign up
    e)  Create twig master template
    f)  Exchange main page codes
2.
    a) Add menu block 
    b) Handle user authentication
    c) change isRegister when user sign up
    d)delete contact us from master
3. Read about creating chart 
    

Marina
1. a) Fill up database with data
    b) Trello user part
    c) Insert the main page into twig template
    d) Add login, signup, book buttons to the main page
    e) Exchange main page codes
    f) Set up the wizard
    g) Step 1 layout
2. a) Fix the menu block and create a function
   b) Finish Step 1 of the wizard (functionality)
   c) Start Step 2
   d) Change the background of a calendar
3. Read about creating a pdf file out of html page




June 26, 2019

Shagha
1.
    a) Add menu block
    b) change isRegister when user sign up
    c) delete contact us from master
    d)  Read about creating chart 
    
2.
    a) Add charts template twig
    b) create query for charts
    c)create sign up successful template twig
    
3. Read about creating calendar

    

Marina
1. a) Fix the menu block
    b) Finish Step 1 of the wizard (functionality)
2. a) Start Step 2
   b) Resolve the css conflict
   c) Modify database field Pickup and Drop date to datetime
3. Read about creating a pdf file out of html page



June 27, 2019

Shagha
1.
    a)Add charts template twig
    b) create query for charts
    c) create sign up successful template twig
    
2.
    a) add condition and fix the literal
    b) put @ session
    c)create calendar template twig 
    d) create query for calendar
    
3. Read about creating calendar

    

Marina
1. a) Implemented Step 2 logic
   b) Implemented Step 3 logic
   c) Modified database
2. a) Create ajax pagination
   b) Implement step 1 and step 2 using Ajax pagination
3. Read about creating a pdf file out of html page


June 28, 2019

Shagha
1.
    a)add condition and fix the literal
    b)  put @ session
    c) try to add calendar!
    
2.
    a)create calendar template twig 
    b) create query for calendar
    c)create management twig template
    
3. Read about creating calendar

    

Marina
1. a) Fixed jQuery problem
   b) Created a long-from booking document
2. a) Create ajax booking twig
   b) Finish step 2 and create step 3 of booking
3. Read about creating a pdf file out of html page



July 2, 2019

Shagha
1.
    a)create calendar template twig 
    b) create query for calendar
    c) create management twig template
    d) Get photos from database in index template twig rather than file
    
2.
    a)Do strip text for my twigs
    b)Presentation (database update ,chart,calendar)   
    d)check title of calendar 
    f)upload project
    g)Do last test
    
3. 

    

Marina
1. a) Finished online booking system for users (registered and not)
   b) Implemented the contact us form handling
   c) Fixed the menu layout and variable passing
2. a) Implement admin can edit rental agreement experience
   b) Test relevant project parts on web hosting
   c) Make steps 2-4 unaccessible from outside of booking system.
   d) Work on presentation
3. 

