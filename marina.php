<?php

if(false){
    $app=new \Slim\Slim();
}

$app->get('/', function() use ($app) {
     $photoList=DB::query("SELECT cars.id, photos.pathPhoto FROM cars, photos WHERE cars.id=photos.carId");    
    
        $app->render('index.html.twig', array('user'=>@$_SESSION['user'],'photos'=>$photoList));
    
});

$app->post('/', function() use ($app) {
    $name = strip_tags($app->request()->post('name'));
    $email = strip_tags($app->request()->post('email'));
    $phone = $app->request()->post('phone');
    $subject = strip_tags($app->request()->post('subject'));
    $message = strip_tags($app->request()->post('message'));
    $checkHuman = strip_tags($app->request()->post('checkHuman'));
    
    if (!$checkHuman){
    $error=false;
    $nameError="";
    $emailError="";
    $phoneError="";
    $subjectError="";
    $messageError="";
    
    if(strlen($name)<2 || strlen($name)>50){
        $error = TRUE;
        $nameError = "Name should be 2-50 characters long";
        $name = "";
    }
        
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        $error = TRUE;
        $email = "";
        $emailError = "Email invalid";
    }
    
    if ($phone && !preg_match("/[0-9]{3}-[0-9]{3}-[0-9]{4}/", $phone)) {
        $error = TRUE;
        $phone = "";
        $phoneError = "Phone number must be like : 555-555-5555";
    }
    
    if (strlen($subject)>100){
        $error = TRUE;
        $subjectError = "Subject shall contain maximum 100 characters.";
        $subject = "";
    }
    
    if (strlen($message)>1000 || strlen($message)<2){
        $error = TRUE;
        $messageError = "Message shall contain maximum 1000 characters.";
        $message = "";
    }

    if ($error){
    $app->render('index.html.twig', array('user'=>@$_SESSION['user'], 'errorList'=>array('name'=>$nameError,
        'email'=>$emailError, 'phone'=>$phoneError, 'subject'=>$subjectError, 'message'=>$messageError), 'v'=>array('name'=>$name,
            'email'=>$email, 'phone'=>$phone, 'subject'=>$subject, 'message'=>$message)));
    } 
    else {
        $address = "marisha.alyokhina@gmail.com";
        $sub = "New message from a client";
        $mes = <<< ENDMARKER
        New message from contact us form.
        ***************************************
        Client`s name: $name 
        Email: $email
        Phone number: $phone
        Subject: $subject
        Message:
        $message
ENDMARKER;
        $from  = "From: $name <$email> \r\n Reply-To: $email \r\n";
        if(mail($address, $sub, $mes, $from)){
            $result="Thank you for your message! We will contact you shortly";
            exit;
        } else {$result="Sorry, something went wrong. Your message cannot be delivered now.";exit;}
        $app->render('index.html.twig', array('user'=>@$_SESSION['user'], 'result'=>$result));
    }
    } else {return;}
});

$app->get('/booking(/:id)', function($id="") use ($app) {
    
    $carsList=DB::query("SELECT cars.id, make, model, year, photos.pathPhoto FROM cars, photos WHERE cars.id=photos.carId AND photos.id>=30 AND photos.id<=36");
    
    $today=date("Y-m-d");
    $tomorrow=date('Y-m-d', strtotime("+1 day"));

    if(!$carsList){
        $app->render("internal_error.html.twig");
        http_response_code(500);
        die();
    } else{
        if ($id!=""){
            $app->render('step1.html.twig', array('carsList' => $carsList, 'today'=>$today, 'tomorrow'=>$tomorrow, 'selectedId'=>$id, 'user'=>@$_SESSION['user']));
        } else{
            $app->render('step1.html.twig', array('carsList' => $carsList, 'today'=>$today, 'tomorrow'=>$tomorrow, 'user'=>@$_SESSION['user']));
        }
    }   
});

$app->post('/booking(/:id)', function($id="") use ($app,$log) {
    $carId=$app->request()->post('carId');
    
    $startDateVal = $app->request()->post('pickUpDate');
    $startDate = date("Y-m-d", strtotime($startDateVal));
    $startTime = $app->request()->post('pickUpTime');
    $returnDateVal=$app->request()->post('returnDate');
    $returnDate = date("Y-m-d", strtotime($returnDateVal));
    $carIdError = '';
    $datesError = '';
    
    $error = FALSE;
    
    if (!$carId){
        $error = TRUE;
        $carIdError = "Please, choose a car";
    }
    
    if ($startDate=="" || $startTime=="" || $returnDate=="" || $returnDate<$startDate){
        $error = TRUE;
        $datesError = "Please, choose pick up date and time and return date, return date cann not be before pick up date";
    }
    
    if ($error) {
        $carsList=DB::query("SELECT cars.id, make, model, year, photos.pathPhoto FROM cars, photos WHERE cars.id=photos.carId AND photos.id>=30 AND photos.id<=36");
        $today=date("Y-m-d");
        $tomorrow=date('Y-m-d', strtotime("+1 day"));

        $app->render('step1.html.twig', array('user'=>@$_SESSION['user'], 'carsList' => $carsList, 'today'=>$today, 'tomorrow'=>$tomorrow, 'errorList' => array('carId' => $carIdError, 'dates' => $datesError),
            'v' => array('pickUpDate' => $startDateVal, 'returnDate'=>$returnDateVal)));
    } else{
        $log->info(sprintf("Rental request for carId=%s, dates from %s, pick-up time %s till %s", $carId, $startDate, $startTime, $returnDate));
        $returnDateTime = $returnDate . " ". ($startTime+3) .":00:00";
        $pickupDateTime = $startDate. " ". ($startTime-3) .":00:00";
        $_SESSION['pickupDateTime']=$pickupDateTime;
        $_SESSION['returnDateTime']=$returnDateTime;
        $_SESSION['chosenCarID']=$carId;
        
        $pickUpDateTaken1 = DB::queryFirstField("SELECT COUNT(id) FROM rentalagreements WHERE carId=%i AND startDate BETWEEN %s AND %s", $carId, $pickupDateTime, $returnDateTime);
        $pickUpDateTaken2 = DB::queryFirstField("SELECT COUNT(id) FROM rentalagreements WHERE carId=%i AND %s BETWEEN startDate AND dueDate", $carId, $pickupDateTime);
        $returnDateTaken1 = DB::queryFirstField("SELECT COUNT(id) FROM rentalagreements WHERE carId=%i AND dueDate BETWEEN %s AND %s", $carId, $pickupDateTime, $returnDateTime);
        $returnDateTaken2 = DB::queryFirstField("SELECT COUNT(id) FROM rentalagreements WHERE carId=%i AND %s BETWEEN startDate AND dueDate", $carId, $returnDateTime);
        
        if($pickUpDateTaken1<1 && $returnDateTaken1<1 && $pickUpDateTaken2<1 && $returnDateTaken2<1){
            $app->redirect('/booking-step3');
        } else{
            $app->redirect('/booking-step2');
        }
    }
});

$app->get('/booking-step2', function() use ($app) {
       if(!isset($_SESSION['chosenCarID'])){
         $app->redirect('/booking');  
       } else{
        $takenCarsList = DB::queryFirstColumn("SELECT DISTINCT carId FROM rentalagreements WHERE startDate BETWEEN %s_pickup AND %s_return OR dueDate BETWEEN %s_pickup AND %s_return OR %s_pickup BETWEEN startDate and dueDate OR %s_return BETWEEN startDate and dueDate", array('pickup'=>@$_SESSION['pickupDateTime'], 'return'=>@$_SESSION['returnDateTime']));
        $takenCarsId = implode("', '", $takenCarsList);
        $availableCarsList=DB::query("SELECT cars.id, make, model, year, photos.pathPhoto FROM cars, photos WHERE cars.id=photos.carId AND photos.id>=30 AND photos.id<=36 AND cars.id NOT IN %ls", array($takenCarsId));
            
        $app->render('step2.html.twig', array('user'=>@$_SESSION['user'], 'availableCarsList'=>$availableCarsList));  

       }
});

$app->post('/booking-step2', function() use ($app) {
        if(!isset($_SESSION['chosenCarID'])){
             $app->redirect('/booking');  
        } else{
        $newCarId=$app->request()->post('newCarId');
        $error="";
        
        if(!$newCarId){
        $error = "Please, choose a car";
        $takenCarsList = DB::queryFirstColumn("SELECT DISTINCT carId FROM rentalagreements WHERE startDate BETWEEN %s_pickup AND %s_return OR dueDate BETWEEN %s_pickup AND %s_return OR %s_pickup BETWEEN startDate and dueDate OR %s_return BETWEEN startDate and dueDate", array('pickup'=>@$_SESSION['pickupDateTime'], 'return'=>@$_SESSION['returnDateTime']));
        $takenCarsId = implode("', '", $takenCarsList);
        $availableCarsList=DB::query("SELECT cars.id, make, model, year, photos.pathPhoto FROM cars, photos WHERE cars.id=photos.carId AND photos.id>=30 AND photos.id<=36 AND cars.id NOT IN %ls", array($takenCarsId));
            
        $app->render('step2.html.twig', array('user'=>@$_SESSION['user'], 'availableCarsList'=>$availableCarsList, 'error' => $error));
        } else{
            $_SESSION['chosenCarID']=$newCarId;
            $app->redirect('/booking-step3');
        }}
});

$app->get('/booking-step3', function() use ($app) {
    if(!isset($_SESSION['chosenCarID'])){
         $app->redirect('/booking');  
       } else{
    if (isset($_SESSION['user'])){
        $user=$_SESSION['user'];
        $app->render('step3.html.twig', array('v'=>$user));
    }
    else {$app->render('step3.html.twig');}
       }
});

$app->post('/booking-step3', function() use ($app, $log) {
    if(!isset($_SESSION['chosenCarID'])){
         $app->redirect('/booking');  
       } else{
    $userFName = strip_tags($app->request()->post('userFName'));
    $userLName = strip_tags($app->request()->post('userLName'));
    $driverLicense = strip_tags($app->request()->post('driverLicense'));
    $insurancePolicy = strip_tags($app->request()->post('insurancePolicy'));
    $address = strip_tags($app->request()->post('address'));
    $city = strip_tags($app->request()->post('city'));
    $province = strip_tags($app->request()->post('province'));
    $postalCode = $app->request()->post('postalCode');
    $country = strip_tags($app->request()->post('country'));
    $phoneNumber = $app->request()->post('phoneNumber');
    $email = strip_tags($app->request()->post('email'));
    $mailerConsent = isset($_POST['mailerConsent']);
    $emailError = '';
    $fNameError = '';
    $lNameError = '';
    $driverLicenseError = '';
    $addressError = '';
    $cityError = '';
    $provinceError = '';
    $postalCodeError = '';
    $countryError = '';
    $phoneNumberError = '';
    $driverLicenseExistError='';
    $error = FALSE;
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        $error = TRUE;
        $email = "";
        $emailError = "Email invalid";
    }

    if (strlen($userFName) < 2 || strlen($userFName) > 50) {
        $error = TRUE;
        $fNameError = "Fisrt name must be 2-50 characters long";
        $userFName = "";
    }
    if (strlen($userLName) < 2 || strlen($userLName) > 50) {
        $error = TRUE;
        $userLName = "";
        $lNameError = "Last name must be 2-50 characters long";
    }
    
    
    if (!($driverLicense) || strlen($driverLicense) >= 16) {
        $error = TRUE;
        $driverLicense = "";
        $driverLicenseError = "Driver license number  must be 15 characters or less";
    } else {
        $userFromDB = DB::queryFirstRow("SELECT * FROM users WHERE driverLicense=%s", $driverLicense);
     }
   
    if ($userFName==$userFromDB['userFName'] && $userLName==$userFromDB['userLName']){
   
        $userExists=$userFromDB;
    } else{
        $error = TRUE;
        $driverLicenseExistError = "Driver license with this number is associated with another first and last names. Please check the spelling. If this driver`s license belong to you, but you cannot continue, please, contact us to clarify the situation.";
    }
    
    
    
    if (!($address) || strlen($address) < 2 || strlen($address) > 100) {
        $error = TRUE;
        $address = "";
        $addressError = "Address must be 2-100 characters long";
    }
    if (!($city) || strlen($city) < 2 || strlen($city) > 50) {
        $error = TRUE;
        $city = "";
        $cityError = "City must be  2-50 characters long";
    }
    if (!($province) || strlen($province) < 2 || strlen($province) > 50) {
        $error = TRUE;
        $province = "";
        $provinceError = "Province must be 2-50 characters long";
    }
    if (!($postalCode) || strlen($postalCode) != 6) {
        $error = TRUE;
        $postalCode = "";
        $postalCodeError = "Postal code must be 6 characters long";
    }
    if (!($country) || strlen($country) < 2 || strlen($country) > 50) {
        $error = TRUE;
        $country = "";
        $countryError = "Country must be 2-50 characters long";
    }
    if (!preg_match("/[0-9]{3}-[0-9]{3}-[0-9]{4}/", $phoneNumber)) {
        $error = TRUE;
        $phoneNumber = "";
        $phoneNumberError = "Phone number must be like : 555-555-5555";
    }
    
    if ($error) { // STATE 2: failed submission
        $app->render('step3.html.twig', array('user'=>@$_SESSION['user'], 
            'errorList' => array('email' => $emailError, 'fName' => $fNameError, 'lName' => $lNameError, 'driverLicense' => $driverLicenseError, 'driverLicenseExist'=>$driverLicenseExistError,
                'address' => $addressError, 'city' => $cityError
                , 'province' => $provinceError, 'postalCode' => $postalCodeError, 'country' => $countryError, 'phoneNumber' => $phoneNumberError,
               ),
            'v' => array('email' => $email, 'userFName' => $userFName, 'userLName' => $userLName, 'driverLicense' => $driverLicense, 
                'insurancePolicy' => $insurancePolicy, 'address' => $address, 'city' => $city
                , 'province' => $province, 'postalCode' => $postalCode, 'country' => $country, 'phoneNumber' => $phoneNumber
                , 'mailerConsent' => $mailerConsent)
        ));
    } else { // STATE 3: successful submission
        if ($userExists){
            DB::update('users', array('email' => $email, 'userFName' => $userFName, 'userLName' => $userLName, 'driverLicense' => $driverLicense,
            'insurancePolicy' => $insurancePolicy, 'address' => $address, 'city' => $city
            , 'province' => $province, 'postalCode' => $postalCode, 'country' => $country, 'phoneNumber' => $phoneNumber
            , 'email' => $email, 'mailerConsent' => $mailerConsent), "id=%s", $userExists['id']);
            $_SESSION['contractUser']=$userExists;
        }
        else if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin']==1  ){
        DB::insert('users', array('email' => $email, 'userFName' => $userFName, 'userLName' => $userLName, 'driverLicense' => $driverLicense,
            'insurancePolicy' => $insurancePolicy, 'address' => $address, 'city' => $city
            , 'province' => $province, 'postalCode' => $postalCode, 'country' => $country, 'phoneNumber' => $phoneNumber
            , 'email' => $email, 'mailerConsent' => $mailerConsent));
        $userId = DB::insertId();
        $_SESSION['contractUser']=DB::queryFirstRow("SELECT * from users where id=%s", $userId);
        } else{
            DB::update('users', array('email' => $email, 'userFName' => $userFName, 'userLName' => $userLName, 'driverLicense' => $driverLicense,
            'insurancePolicy' => $insurancePolicy, 'address' => $address, 'city' => $city
            , 'province' => $province, 'postalCode' => $postalCode, 'country' => $country, 'phoneNumber' => $phoneNumber
            , 'email' => $email, 'mailerConsent' => $mailerConsent), "id=%s", $_SESSION['user']['id']);
        }
        $app->redirect('/booking-step4');
       }}
});

$app->get('/booking-step4', function() use ($app) {
    if(!isset($_SESSION['chosenCarID'])){
         $app->redirect('/booking');  
       } else{
    $user= isset($_SESSION['user']) ? $_SESSION['user'] : $_SESSION['contractUser'];
    $fName=$user['userFName'];
    $lName=$user['userLName'];
    $car=DB::queryFirstRow("SELECT * FROM cars where id=%s", @$_SESSION['chosenCarID']);
    $make=$car['make'];
    $model=$car['model'];
    $year=$car['year'];
    $VIN=$car['VIN'];
    $color=$car['color'];
    $pickupDateTime=$_SESSION['pickupDateTime'];
    $returnDateTime=$_SESSION['returnDateTime'];
    $dailyFee=$car['dailyFee'];
    $deposit=$car['deposit'];
    
    $app->render('step4.html.twig', array('user'=>@$_SESSION['user'], 'fName'=>$fName, 'lName'=>$lName, 'make'=>$make, 'model'=>$model, 
        'year'=>$year, 'VIN'=>$VIN, 'color'=>$color, 'pickupDateTime'=>$pickupDateTime, 
        'returnDateTime'=>$returnDateTime, 'dailyFee'=>$dailyFee, 'deposit'=>$deposit));
       }
});

$app->post('/booking-step4', function() use ($app, $log) {
    if(!isset($_SESSION['chosenCarID'])){
         $app->redirect('/booking');  
       } else{
    $postTripCleaning = $app->request()->post('postTripCleaning');
    if ($postTripCleaning!=1) $postTripCleaning=0;
    $prepaidFuel = $app->request()->post('prepaidFuel');
    if ($prepaidFuel!=1) $prepaidFuel=0;
    $acceptContract=$app->request()->post('acceptContract');
    $insuranceType=$app->request()->post('insuranceType');
    $error=false;
    $insuranceError="";
    $acceptError="";
    
    if ($insuranceType==""){
        $error=true;
        $insuranceError = "Please, choose the type of insurance policy you want to use.";
    }
    
    if ($acceptContract!="accept"){
        $error=true;
        $acceptError = "To continue, please accept the terms and conditions.";
    }
    
    if($error){
        $user= isset($_SESSION['user']) ? $_SESSION['user'] : $_SESSION['contractUser'];
        $fName=$user['userFName'];
        $lName=$user['userLName'];
        $car=DB::queryFirstRow("SELECT * FROM cars where id=%s", @$_SESSION['chosenCarID']);
        $make=$car['make'];
        $model=$car['model'];
        $year=$car['year'];
        $VIN=$car['VIN'];
        $color=$car['color'];
        $pickupDateTime=$_SESSION['pickupDateTime'];
        $returnDateTime=$_SESSION['returnDateTime'];
        $dailyFee=$car['dailyFee'];
        $deposit=$car['deposit'];
        $app->render('step4.html.twig', array('user'=>@$_SESSION['user'], 'fName'=>$fName, 'lName'=>$lName, 'make'=>$make, 'model'=>$model, 
        'year'=>$year, 'VIN'=>$VIN, 'color'=>$color, 'pickupDateTime'=>$pickupDateTime, 
        'returnDateTime'=>$returnDateTime, 'dailyFee'=>$dailyFee, 'deposit'=>$deposit, 'errorList'=>array('insuranceType'=>$insuranceError, 'acceptContract'=>$acceptError)));
    } else {
        $user= isset($_SESSION['user']) ? $_SESSION['user'] : $_SESSION['contractUser'];
        DB::insert('rentalagreements', array('carId'=>$_SESSION['chosenCarID'], 'userId'=>$user['id'],
            'startDate'=>$_SESSION['pickupDateTime'], 'dueDate'=>$_SESSION['returnDateTime'], 'insuranceType'=>$insuranceType, 
            'postTripCleaning'=>$postTripCleaning, 'prepaidFuel'=>$prepaidFuel));
        $log->info(sprintf("Contract created, carId = %s, start date =%s, return date =%s, client name=%s %s", $_SESSION['chosenCarID'], $_SESSION['pickupDateTime'], $_SESSION['returnDateTime'], $user['userFName'], $user['userLName']));
        $app->redirect('/booking-success');
    }
}
});

$app->get('/booking-success', function() use ($app) {
    if(!isset($_SESSION['chosenCarID'])){
         $app->redirect('/booking');  
       } else{
    $app->render('booking_success.html.twig', array('user'=>@$_SESSION['user']));
       }
});
