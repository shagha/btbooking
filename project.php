<?php


// user: cp4928
//password: wnom6m4q
//




session_cache_limiter(false);
session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));





if ($_SERVER['SERVER_NAME'] == 'localhost') {
    DB::$user = 'btluxury';
    DB::$password = 'ipdshaghayegh';
    DB::$dbName = 'btluxury';
    DB::$encoding = 'utf8';
    DB::$port = 3333;
} else {

    DB::$user = 'cp4928_btluxury';
    DB::$password = 'bjWefFtzlDmf';
    DB::$dbName = 'cp4928_btluxury';
    DB::$encoding = 'utf8';
}
//bjWefFtzlDmf



DB::$error_handler = 'database_error_handler';
DB::$nonsql_error_handler = 'database_error_handler';





function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }
    $app->render("internal_error.html.twig");
    http_response_code(500);
    die(); // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$app->response()->header('Content-Type: application/json');

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9][0-9]*'
));

function getUserIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

require_once 'marina.php';

// STATE 1: first show
$app->get('/login', function() use ($app) {
    if (isset($_SESSION['user'])) {
        $app->render('login.html.twig', array('user' => @$_SESSION['user']));
    } else
        $app->render('login.html.twig');
});

$app->post('/login', function() use ($app, $log) {
    $email =  strip_tags($app->request()->post('email'));
    $password =  strip_tags($app->request()->post('password'));
    //
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    //
    if (!$loginSuccessful) { // array not empty -> errors present
        $log->info(sprintf("Login failed, email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login.html.twig', array('error' => true));
    } else { // STATE 3: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $log->info(sprintf("Login successful, email=%s, from IP=%s", $email, getUserIpAddr()));
        if ($_SESSION['user']['isAdmin'])
            $app->render('management.html.twig', array('user' => @$_SESSION['user']));
        else
            $app->render('index.html.twig', array('user' => @$_SESSION['user']));
    }
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['user']);
    $app->render('index.html.twig');
});
$app->get('/management', function() use ($app) {
    if (isset($_SESSION['user'])) {
        $app->render('management.html.twig', array('user' => @$_SESSION['user']));
    } else
         $app->render('management.html.twig');
   
});
//test logout
$app->get('/session', function() {
    echo '<pre>';
    print_r($_SESSION);
});

$app->get('/isemailregistered/(:email)', function($email = "") use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) {
        echo "Email already registered";
    }
});

$app->get('/signup', function() use ($app) {
    if (isset($_SESSION['user'])) {
        $app->render('signup.html.twig', array('user' => @$_SESSION['user']));
    } else
        $app->render('signup.html.twig');
});

$app->post('/signup', function() use ($app, $log) {
    $email =  strip_tags($app->request()->post('email'));
    $pass1 =  strip_tags($app->request()->post('pass1'));
    $pass2 =  strip_tags($app->request()->post('pass2'));
    $pass1Error = '';
    $pass2Error = '';
    $emailError = '';

    $error = FALSE;
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        $error = TRUE;
        $email = "";
        $emailError = "Email invalid";
    } else {
        $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($user) {
            $error = TRUE;
            $emailError = "Email alredy exist.";
        }
    }
    if ($pass1 != $pass2) {
        $error = TRUE;
        $pass2Error = "Passwords do not match";
    } else {
        if ((strlen($pass1) < 6) || (preg_match("/[A-Z]/", $pass1) == FALSE ) || (preg_match("/[a-z]/", $pass1) == FALSE ) || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            $error = TRUE;
            $pass1Error = "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }
    if ($error) { // STATE 2: failed submission
        $email =  strip_tags($app->request()->post('email'));
        $pass1 =  strip_tags($app->request()->post('pass1'));
        $pass2 = strip_tags( $app->request()->post('pass2'));

        $app->render('signup.html.twig', array(
            'errorList' => array('email' => $emailError, 'pass1' => $pass1Error, 'pass2' => $pass2Error),
            'v' => array('email' => $email)
        ));
    } else { // STATE 3: successful submission
        DB::insert('users', array('email' => $email, 'password' => $pass1, 'isRegistered' => TRUE));
        $userId = DB::insertId();
        $log->debug("User registed with id=" . $userId);
        $app->render('signup_success.html.twig', array('user' => @$_SESSION['user']));
    }
});

//===========Charts=================




use Khill\Lavacharts\Lavacharts;

$monday = 0;
$tuesday = 0;
$wednsday = 0;
$thursday = 0;
$friday = 0;
$saturday = 0;
$sunday = 0;
function addWeekDays($result){
    global $monday,$tuesday,
$wednsday,
$thursday,
$friday ,
$saturday ,
$sunday ;
    
    foreach ($result as $row) {
        switch ($row['dayIndex']) {
            case 0:
                $monday += $row['count'];
                break;
            case 1:
                $tuesday += $row['count'];
                break;
            case 2:
                $wednsday += $row['count'];
                break;
            case 3:
                $thursday += $row['count'];
                break;
            case 4:
                $friday += $row['count'];
                break;
            case 5:
                $saturday += $row['count'];
                break;
            case 6:
                $sunday += $row['count'];
                break;

            default:
            // Code to be executed if n is different from all labels
        }
    }
   
}


$app->get('/chart', function() use ($app) {
   
    if (isset($_SESSION['user'])) {
        $app->render('charts.html.twig', array('user' => @$_SESSION['user']));
    } else
        $app->render('charts.html.twig');
});
$app->get('/firstchart/(:month)', function($month = 0) use ($app) {

 global $monday,$tuesday,
$wednsday,
$thursday,
$friday ,
$saturday ,
$sunday ;

    $counPerDayOfWeek = DB::query("SELECT count(id) as count,weekDay(dueDate)as dayIndex FROM rentalagreements WHERE MONTH(dueDate) =%l and YEAR(dueDate) = YEAR(CURDATE()) group by WeekDay(dueDate)",$month);
    $monday = 0;
    $tuesday = 0;
    $wednsday = 0;
    $thursday = 0;
    $friday = 0;
    $saturday = 0;
    $sunday = 0;
    addWeekDays($counPerDayOfWeek);
    $counPerDayOfWeek = DB::query("SELECT count(id)  as count,weekDay(returnDate) as dayIndex FROM `rentalagreements` WHERE MONTH(returnDate) =%l and YEAR(dueDate) = YEAR(CURDATE()) group by WeekDay(returnDate)",$month);
   addWeekDays($counPerDayOfWeek);

    $counPerDayOfWeek = DB::query("SELECT count(id) as count,weekDay(startDate)as dayIndex FROM `rentalagreements` WHERE MONTH(startDate) =%l and YEAR(dueDate) = YEAR(CURDATE()) group by WeekDay(startDate)",$month);
    addWeekDays($counPerDayOfWeek);


    $lava = new Lavacharts();
    $bar = $lava->DataTable();
    $bar->addStringColumn('Day of wek')
            ->addNumberColumn('Number of rent')
            ->addRow(array('Monday', $monday))
            ->addRow(array('Tuesday', $tuesday))
            ->addRow(array('Wednesday', $wednsday))
            ->addRow(array('Thursday', $thursday))
            ->addRow(array('Friday', $friday))
            ->addRow(array('Saturday', $saturday))
            ->addRow(array('Sunday', $sunday));
    $lava->BarChart('chart', $bar);

    echo $lava->render('BarChart', 'chart', 'chart-div', ['width' => 300, 'height' => 300]);

   
})->conditions(array('month' => '[1-9][1-2]*'));
//====================Calendar

$app->get('/calendrierF', function() use ($app) {
    
       $rentalList= DB::query("SELECT c.make,c.model ,r.startDate,r.dueDate FROM `rentalagreements` as r,`cars` as c WHERE c.id= r.carId");
       $carAvailibility =array();
       
       $i = 1;
foreach ($rentalList as $value){
    array_push($carAvailibility,  Array
        (
            "id"=>$i++,
            "title" => $value['make']." ".$value['model'],
        
           
            "start" => $value['startDate'],
           
             "end" => $value['dueDate'],
           
            "color" => "red"
           
        ));
}
     
         echo json_encode($carAvailibility, JSON_PRETTY_PRINT);
});

$app->get('/calendar', function() use ($app,$log) {
    
     if (isset($_SESSION['user'])) {
        $app->render('calendar.html.twig', array('user' => @$_SESSION['user']));
    } else    
    $app->render("calendar.html.twig");
 });
$app->run();

